﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;



namespace GarpOrderMgr
{
    public partial class Mainform : Form
    {
        bool doit = false;
        //LoggMgr  = new LoggMgr();
        GarpMgr m_Garphelper = new GarpMgr();
        List<ArticleDTO> m_articleLevList;

        Dictionary<string, string> comboSourceCustomer = new Dictionary<string, string>();
        Dictionary<string, string> comboSourceProduct = new Dictionary<string, string>();
        //Dictionary<string, string> customerNoNameMapping = new Dictionary<string, string>();
        //Dictionary<string, string> articleNoNameMapping = new Dictionary<string, string>();
        //Dictionary<string, string> customerNoMapping = new Dictionary<string, string>();
        //Dictionary<string, string> articleNoMapping = new Dictionary<string, string>();


        string m_activefile = "";
        //string m_kundNr = "";
        
        DateTime m_dateDaysAheadForecast;

        public Mainform()
        {
            InitializeComponent();
        }

        private void Mainform_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            cmdSave.Enabled = false;
            initControls();
            
            //loadMappingStructuresFromFile();
            loadGISCustomers();
            loadGISArticles();
            onStart();
            cmdSave.Enabled = true;
            configGridRows();
            dgvOrder.Columns[11].Visible = !Properties.Settings.Default.hideColPlan;
        }

        private void configGridRows()
        {
            int numVisibleRowsGrid = 0;
            int numOutSideHorisontRows = 0;
            //delete rows should default to checked
            foreach (DataGridViewRow row in dgvOrder.Rows)
            {
                string deldate = row.Cells["Levtid"].Value.ToString();
                DateTime levDate = DateTime.ParseExact(deldate, "yyMMdd", CultureInfo.CurrentCulture);
                DataGridViewCell dgvActionCell = row.Cells["Atgard"];
                DataGridViewCell dgvStatusCell = row.Cells["Status"];
                DataGridViewCell dgvAntalCell = row.Cells["Antal"];

                if (levDate.Date > m_dateDaysAheadForecast.Date && Properties.Settings.Default.hideRowsOutsideHorisont)
                {
                    //Alla rader som ligger bortom forecast datumet skall vi dölja i GUI, dock skall vi agera på de raderna vid spara så de skall finnas i griden under ytan
                    row.Visible = false;
                    numOutSideHorisontRows++;
                }

                if (dgvActionCell.Value.ToString() != "" && dgvStatusCell.Value.ToString() != Program.g_STATUS_CUST_NOT_FOUND && dgvStatusCell.Value.ToString() != Program.g_STATUS_ART_NOT_FOUND)
                {
                    //CHANGE checkboxen visas för denna rad

                    bool newQuantZero = false;
                    //Justering antal sker ej automatiskt->OM programmet föreslår ett nytt antal på en rad, t.ex.om det i Garp var 480 och det står 360 i filen skall ”Change” – rutan vara ibockad per default(Om den är synlig).
                    //Det gäller inte om antalet ändras till 0, exempel 480-> 0     
                    if(dgvActionCell.Value.ToString().IndexOf(Program.g_ACTION_NEWQUANTITY) != -1)
                    {
                        //det är en new quantity row
                        //kolla om den vill sätta till antal 0
                        if(dgvAntalCell.Value.ToString().IndexOf("-> 0") != -1)
                        {
                            newQuantZero = true;
                        }
                    }

                    //Gör ingen automatisk ibockning om Dåtid eller new quantity -> 0
                    if (!isInPresentTime(row) && !newQuantZero)
                    {
                        row.Cells["Andra"].Value = 1;
                    }
                }

                if (dgvStatusCell.Value.ToString() == Program.g_STATUS_PROGNOS || dgvActionCell.Value.ToString() == Program.g_ACTION_NEWORDERROW || dgvActionCell.Value.ToString() == Program.g_ACTION_NEWORDER)
                {
                    //PLAN checkboxen visas för denna rad
                    if (levDate.Date >= DateTime.Now.Date && levDate.Date <= m_dateDaysAheadForecast.Date)
                    {
                        //Period 2: Inom forecast tid
                        //Bocka i plan
                        row.Cells["Plan"].Value = 1;

                        if (dgvStatusCell.Value.ToString() == Program.g_STATUS_PROGNOS && dgvActionCell.Value.ToString() == "")
                        {
                            //På rader som ligger inom 3 veckor och som idag endast har en "plan" ruta skall den automatiskt bockas i. Status = Forecast, Action = ""
                            row.Cells["Atgard"].Value = Program.g_STATUS_PROGNOS + "->Normal order";
                        }
                    }
                }
                
                // All Changes (except for DELETE) in present time should not be active (eg. Change is NOT checked)
                if (dgvActionCell.Value.ToString() == Program.g_ACTION_NEWORDER || dgvActionCell.Value.ToString() == Program.g_ACTION_NEWORDERROW || dgvActionCell.Value.ToString().StartsWith(Program.g_ACTION_NEWQUANTITY))
                {
                    // 
                    if (isInPresentTime(row))
                    {
                        row.Cells["Andra"].Value = 0;
                        row.Cells["Plan"].Value = 0;
                    }
                    else
                    {
                        row.Cells["Andra"].Value = 1;
                    }
                }
                // If DELETE then we allow Change to be checked, if NOT "Planned", then we uncheck Check box
                else if(dgvActionCell.Value.ToString() == Program.g_ACTION_DELETE)
                {
                    
                    if (isInPresentTime(row) && dgvStatusCell.Value.ToString() == Program.g_STATUS_PLANNED)
                    {
                        row.Cells["Andra"].Value = 0;
                        row.Cells["Plan"].Value = 0;
                    }
                    else
                    {
                        row.Cells["Andra"].Value = 1;
                    }
                }

                if (row.Visible)
                {
                    numVisibleRowsGrid++;
                }
            }

            lblInfo.Text = "Number of rows: " + numVisibleRowsGrid;
        }


        private int getIdxArtLevListForGridRow(DataGridViewRow row, List<ArticleDTO> articleLevList)
        {
            for (int i = 0; i < articleLevList.Count; i++)
            {

                if (articleLevList[i].Rowid_xml == Convert.ToInt32(row.Cells["ID"].Value))
                {
                    return i;
                }
            }
            return -1;
        }



        private void loadGISCustomers()
        {
            lblInfo.Text = "Load all customers, please wait...";
            Application.DoEvents();
            DataHandler.GIS_Customers = m_Garphelper.getGISCustomers();
        }


        private void loadGISArticles()
        {
            lblInfo.Text = "Load all articles, please wait...";
            Application.DoEvents();
            DataHandler.GIS_Products = m_Garphelper.getGISArticles();
        }

        private ProductDTO getArticleFromGISList(string artno)
        {
            foreach (ProductDTO art in DataHandler.GIS_Products)
            {
                if (artno == art.ProductNo)
                {
                    return art;
                }
                //Kundens artno är i tx8 fältet
                else if (art.ProductText != null && art.ProductText.Count > 7 && artno == art.ProductText[7].Value)
                {
                    return art;
                }
            }
            return null;
        }


        private void initControls()
        {
            try
            {
                var version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
                this.Text = String.Format("Garp order management {0}", version);

                dgvOrder.Columns["Andra"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvOrder.Columns["Plan"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                int daysAheadForecast = Properties.Settings.Default.daysAheadForeCast;
                m_dateDaysAheadForecast = DateTime.Now.AddDays(daysAheadForecast);
            }
            catch (Exception e)
            {
                LoggMgr.WriteToLogg(System.Reflection.MethodBase.GetCurrentMethod().Name, e);
            }

        }

        private void Mainform_Resize(object sender, EventArgs e)
        {
            initControls();
        }


        private List<ArticleDTO> processEDIFile(string file)
        {
            List<ArticleDTO> result = new List<ArticleDTO>();

            doit = false;     
            m_activefile = file;

            try
            {
                if (file.Length > 0)
                {
                     lblInfo.Text = "Loading file, please wait...";
                    Application.DoEvents();

                    result = readTheFile(file);
                }
            }

            catch (IOException io)
            {
                LoggMgr.WriteToLogg("ERR " + System.Reflection.MethodBase.GetCurrentMethod().Name, io);
                lblInfo.Text = "";
            }

           doit = true;

            return result;
        }


        private void onStart()
        {
            try
            {
                List<ArticleDTO> tempArticleList = new List<ArticleDTO>();
                lblInfo.Text = "Searching for files";
                Application.DoEvents();

                //m_Garphelper.m_GisOrderObj = null;
                m_articleLevList = new List<ArticleDTO>();
                dgvOrder.Rows.Clear();

                string inFolder = Properties.Settings.Default.infolder;
                string inFilePrefix = Properties.Settings.Default.fileprefix;

                DirectoryInfo d = new DirectoryInfo(inFolder);
                FileInfo[] Files = d.GetFiles(inFilePrefix + "*")?.OrderByDescending(fi => fi.LastWriteTime).ToArray();

                if (Files.Length == 0)
                {
                    lblInfo.Text = "No files found";
                    Application.DoEvents();
                    return;
                }

                foreach (FileInfo file in Files)
                {
                    string m_activefile = file.FullName;
                    tempArticleList.Clear();
                    tempArticleList.AddRange(processEDIFile(m_activefile));

                    //loop through our list and add Garp info to each articlelev objects 
                    m_articleLevList.AddRange(getGarpDetails(tempArticleList, lblInfo));
                }


                if (m_articleLevList == null)
                {
                    LoggMgr.WriteToLogg("Canceling because something went wrong in getGarpDetails", null);
                    return;
                }

                //give them an id so we can find them later even if we reorder/filter grid
                for (int i = 0; i < m_articleLevList.Count; i++)
                {
                    m_articleLevList[i].Rowid_xml= i;
                }

                //sort our list on custNo, articlenum, levdate
                if (sortArtLevList())
                {
                    //success reading the file and adding Garp info so add everything to our gridview
                    if (!populateGrid())
                    {
                        LoggMgr.WriteToLogg("Canceling because something went wrong in populateGrid", null);
                    }
                }
                else
                {
                    LoggMgr.WriteToLogg("Canceling because something went wrong in sortArtLevList", null);
                }
                populateProductFilter();
                populateCustomerFilter();
            }
            catch (Exception ex)
            {
                LoggMgr.WriteToLogg("ERR " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                lblInfo.Text = "";
            }

        }


        private List<ArticleDTO> readTheFile(string infile)
        {
            List<ArticleDTO> result = new List<ArticleDTO>();

            try
            {
                if (Path.GetExtension(infile).ToLower() == ".txt")
                {
                    result.AddRange(FileParser.CreateArticlesFromFile(infile));
                }
            }
            catch(Exception e)
            {
                LoggMgr.WriteToLogg("Error getting ArticleDTO from file", e);
            }

            return result;
        }



        //ascending articlenum, descending levdate_xml
        private bool sortArtLevList()
        {
            try
            {
                LoggMgr.debugIt("DEBUG " + System.Reflection.MethodBase.GetCurrentMethod().Name);
                m_articleLevList.Sort(delegate (ArticleDTO p1, ArticleDTO p2)
                {
                    int compareCustNr = (string.IsNullOrEmpty(p1.Kundnr_garp) ? "" : p1.Kundnr_garp).CompareTo((string.IsNullOrEmpty(p2.Kundnr_garp) ? "" : p2.Kundnr_garp));

                    if (compareCustNr == 0)
                    {
                        compareCustNr = (p1.Artnr_xml ?? "").CompareTo(p2.Artnr_xml ?? "");
                        if (compareCustNr == 0)
                        {
                            return p1.Levtid_xml.CompareTo(p2.Levtid_xml);
                        }
                    }

                    return compareCustNr;
                }               
                );

              return true;
            }
            catch (Exception e)
            {
                LoggMgr.WriteToLogg("ERR " + System.Reflection.MethodBase.GetCurrentMethod().Name, e);
                return false;
            }
        }



        private List<ArticleDTO> getGarpDetails(List<ArticleDTO> articleList, Label infoLabel)
        {
            List<ArticleDTO> result = new List<ArticleDTO>();

            try
            {
                LoggMgr.debugIt("DEBUG " + System.Reflection.MethodBase.GetCurrentMethod().Name);

                foreach(string customer in DataHandler.CustomersFoundInFiles )
                {
                    result.AddRange(m_Garphelper.addGarpOrderData(articleList.Where(a=>a.Kundnr_garp == customer)?.ToList(), infoLabel, customer));

                    if (result == null)
                    {
                        LoggMgr.WriteToLogg("Canceling because something went wrong in populateGarpData", null);
                    }
                }
            }
            catch (Exception e)
            {
                LoggMgr.WriteToLogg("ERR " + System.Reflection.MethodBase.GetCurrentMethod().Name, e);
            }

            return result;
        }


        /// <summary>
        /// Populate Grid from productionplan. This is the man data fill to grid, som values are then changed in configGridRows() function and CELLPainteing() event. 
        /// This is at major MESSUP, some real cleanup have to be done here!!!
        /// </summary>
        /// <returns></returns>
        private bool populateGrid()
        {
            try
            {
                LoggMgr.debugIt("DEBUG " + System.Reflection.MethodBase.GetCurrentMethod().Name);
                lblInfo.Text = "Loading datagrid, please wait...";
                Application.DoEvents();
                int numchanges = 0;
                int totcount = m_articleLevList.Count;
          
               for (int i = 0; i < m_articleLevList.Count; i++)
                {
                   
                    if (m_articleLevList[i].Atgardtext != "")
                    {
                        numchanges += 1;
                    }

                    string antalstext = "";
                    if(m_articleLevList[i].Antal_garp != m_articleLevList[i].Antal_xml && m_articleLevList[i].Antal_garp != -1)
                    {
                        antalstext = m_articleLevList[i].Antal_garp.ToString() + " -> " + m_articleLevList[i].Antal_xml.ToString();
                    }
                    else
                    {
                        antalstext = m_articleLevList[i].Antal_xml.ToString();
                    }

                    string orderinfo = "";

                    orderinfo = m_articleLevList[i].Ordernr;

                    if (m_articleLevList[i].Radnr_garp != "")
                    {
                        orderinfo += "-" + m_articleLevList[i].Radnr_garp;
                    }


                    string status = m_articleLevList[i].Statustext;
                    if (status == Program.g_STATUS_PROGNOS && m_articleLevList[i].Atgardtext == Program.g_ACTION_NEWORDERROW)
                    {
                        //Visa ej Forecast om det är ny orderrad
                        status = "";
                    }

                    int rowidx = dgvOrder.Rows.Add(m_articleLevList[i].Rowid_xml, m_articleLevList[i].Kundnr_garp, m_articleLevList[i].Kundname_xml, m_articleLevList[i].Artnr_xml, m_articleLevList[i].Week_xml, m_articleLevList[i].Levtid_xml, antalstext, orderinfo, status, m_articleLevList[i].Atgardtext, false,false);

                    //DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)dgvOrder.Rows[rowidx].Cells["Andra"];

                    //if (chk.Value == chk.TrueValue)
                    //{
                    //    chk.Value = chk.FalseValue;
                    //}

                    //if (m_articleLevList[i].Atgardtext == Program.g_ACTION_DELETE && m_articleLevList[i].Statustext == Program.g_STATUS_PLANNED)
                    //{
                    //    //DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)dgvOrder.Rows[rowidx].Cells["Andra"];
                    //    //chk.Value = chk.FalseValue;
                    //}
                    //else
                    //{
                    //    //DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)dgvOrder.Rows[rowidx].Cells["Andra"];
                    //    //chk.Value = chk.TrueValue;
                    //}

                    dgvOrder.Rows[rowidx].MinimumHeight = 32;

                    lblInfo.Text =  i + " (of " + totcount + ")";

            }
                lblInfo.Text = " Files have been parsed. Found " + numchanges + " changes";

                return true;
            }
            catch (Exception e)
            {
                LoggMgr.WriteToLogg("ERR " + System.Reflection.MethodBase.GetCurrentMethod().Name, e);
                return false;
            }  
        }



        //This method is called for each cell in the grid
        //hide checkboxes for those rows that we have no action to perform
        private void dgvOrder_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
                {
                    // are we painting Andra(change) column?
                    if (dgvOrder.Columns[e.ColumnIndex].Name == "Andra")
                    {
                        if (dgvOrder.Rows[e.RowIndex].Cells["Atgard"].Value.ToString().IndexOf("Normal order") != -1 || dgvOrder.Rows[e.RowIndex].Cells["Atgard"].Value.ToString() == "" || dgvOrder.Rows[e.RowIndex].Cells["Status"].Value.ToString() == Program.g_STATUS_CUST_NOT_FOUND || dgvOrder.Rows[e.RowIndex].Cells["Status"].Value.ToString() == Program.g_STATUS_ART_NOT_FOUND || dgvOrder.Rows[e.RowIndex].Cells["Status"].Value.ToString() == Program.g_STATUS_PLANNED)
                        {
                            e.PaintBackground(e.ClipBounds, true);
                            e.Handled = true;
                        }
                        else
                        {
                            if (true || dgvOrder.Rows[e.RowIndex].Cells["Status"].Value.ToString() == Program.g_STATUS_PROGNOS || dgvOrder.Rows[e.RowIndex].Cells["Atgard"].Value.ToString() == Program.g_ACTION_DELETE)
                            {
                                e.PaintBackground(e.CellBounds, true);
                                ControlPaint.DrawCheckBox(e.Graphics, e.CellBounds.X + 1, e.CellBounds.Y + 1, e.CellBounds.Width - 4, e.CellBounds.Height - 4, (bool)e.FormattedValue ? ButtonState.Checked : ButtonState.Normal);
                                e.Handled = true;
                            }
                            else
                            {
                                e.PaintBackground(e.ClipBounds, true);
                                e.Handled = true;
                            }
                        }
                    }
                    //are we painting Plan column? show checkbox only if forecast
                    else if (dgvOrder.Columns[e.ColumnIndex].Name == "Plan")
                    {
                        if (dgvOrder.Rows[e.RowIndex].Cells["Status"].Value.ToString() == Program.g_STATUS_PROGNOS || dgvOrder.Rows[e.RowIndex].Cells["Atgard"].Value.ToString() == Program.g_ACTION_NEWORDERROW || (dgvOrder.Rows[e.RowIndex].Cells["Atgard"].Value.ToString() == Program.g_ACTION_NEWORDER && isoutsidehorisont(dgvOrder.Rows[e.RowIndex]) == false))
                        {
                            e.PaintBackground(e.CellBounds, true);
                            ControlPaint.DrawCheckBox(e.Graphics, e.CellBounds.X + 1, e.CellBounds.Y + 1, e.CellBounds.Width - 4, e.CellBounds.Height - 4, (bool)e.FormattedValue ? ButtonState.Checked : ButtonState.Normal);
                            e.Handled = true;
                        }
                        else
                        {
                            e.PaintBackground(e.ClipBounds, true);
                            e.Handled = true;
                        }
                    }

                    //Check if we should change background of cell
                    if (dgvOrder.Rows[e.RowIndex].Cells["Atgard"].Value.ToString() != "")
                    {
                        string atgardcleaned = dgvOrder.Rows[e.RowIndex].Cells["Atgard"].Value.ToString();
                        Color colororange;

                        //is there any antals data so remove before comparison
                        if (atgardcleaned.IndexOf("(") > 0)
                        {
                            atgardcleaned = atgardcleaned.Split('(')[0].Trim();
                        }

                        // Check if planned, then we can't delete this row, mark it red and rremove checkbox
                        if (dgvOrder.Rows[e.RowIndex].Cells["Status"].Value.ToString() == Program.g_STATUS_PLANNED)
                        {
                            colororange = Color.Red;
                        }
                        else
                        {
                            colororange = ColorTranslator.FromHtml(Properties.Settings.Default.colorDeleteOrder);
                        }

                        switch (atgardcleaned)
                        {
                            case Program.g_ACTION_NEWORDER:
                            case Program.g_ACTION_NEWORDERROW:
                            case Program.g_ACTION_NEWQUANTITY:
                                //green bg
                                Color colorgreen = ColorTranslator.FromHtml(Properties.Settings.Default.colorNewOrder);
                                dgvOrder.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = colorgreen;
                                break;
                            case Program.g_ACTION_DELETE:
                                //orange bg


                                dgvOrder.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = colororange;
                                break;
                        }
                    }
                    //are we painting a row with missing customer or article?
                    else if (dgvOrder.Rows[e.RowIndex].Cells["Status"].Value.ToString() == Program.g_STATUS_CUST_NOT_FOUND)
                    {
                        Color colorred = ColorTranslator.FromHtml(Properties.Settings.Default.colorCustNotFound);
                        dgvOrder.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = colorred;
                    }
                    else if (dgvOrder.Rows[e.RowIndex].Cells["Status"].Value.ToString() == Program.g_STATUS_ART_NOT_FOUND)
                    {
                        Color colorred = ColorTranslator.FromHtml(Properties.Settings.Default.colorArtNotFound);
                        dgvOrder.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = colorred;
                    }
                    else
                    {
                        //default BG
                        Color colordefault;

                        // Check if planned, then we can't delete this row, mark it red and rremove checkbox
                        if (dgvOrder.Rows[e.RowIndex].Cells["Status"].Value.ToString() == Program.g_STATUS_PLANNED)
                        {
                            colordefault = Color.Red;
                        }
                        else
                        {
                            colordefault = ColorTranslator.FromHtml(Properties.Settings.Default.colorDefaultRows);
                        }

                        dgvOrder.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = colordefault;
                    }


                    string deldate = dgvOrder.Rows[e.RowIndex].Cells["Levtid"].Value.ToString();
                    DateTime levDate = DateTime.ParseExact(deldate, "yyMMdd", CultureInfo.CurrentCulture);

                    if (levDate > m_dateDaysAheadForecast.Date)
                    {
                        Color color = ColorTranslator.FromHtml(Properties.Settings.Default.colorRowsOutsideHorisont);
                        Font myFont = new Font(dgvOrder.DefaultCellStyle.Font.FontFamily, dgvOrder.DefaultCellStyle.Font.Size, FontStyle.Bold);

                        dgvOrder.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.ForeColor = color;
                        //dgvOrder.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.Font = myFont;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggMgr.WriteToLogg("ERR " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
            }
        }
        private void dgvOrder_OnCellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 10 && e.RowIndex != -1)
            {
                if(!isoutsidehorisont(dgvOrder.Rows[e.RowIndex]))
                {
                    dgvOrder.Rows[e.RowIndex].Cells["Plan"].Value = dgvOrder.Rows[e.RowIndex].Cells["Andra"].Value;
                }
            }
        }

        private void dgvOrder_OnCellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            // End of edition on each click on column of checkbox
            if (e.ColumnIndex == 10 && e.RowIndex != -1)
            {
                dgvOrder.EndEdit();
            }
        }

        private void dgvOrder_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                this.dgvOrder.ClearSelection();
            }
            catch (Exception ex)
            {
                LoggMgr.WriteToLogg("ERR " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                lblInfo.Text = "Saving to garp...";
                Application.DoEvents();

                m_Garphelper.clearNewlyAddedOrders();
                m_Garphelper.clearStoredModifiedOnrs();
                m_Garphelper.init();
                bool hasmodifiedGarp = false;
                string donestring = "[DONE]";

                //TODO: Loop through grid and make changes to orders in Garp
                foreach (DataGridViewRow row in dgvOrder.Rows)
                {
                    if (row.Visible || isoutsidehorisontAndValid(row))
                    {
                        //A row we need to do something with?
                        if ((row.Cells["Andra"].Value.ToString() == "1" || row.Cells["Plan"].Value.ToString() == "1") && row.Cells["Week"].Value.ToString().IndexOf(donestring) == -1)
                        {
                            if (!m_Garphelper.ProcessDataGridViewRow(row, m_articleLevList))
                            {
                                LoggMgr.WriteToLogg("Could not save row " + row.Cells["ID"].Value.ToString() + System.Reflection.MethodBase.GetCurrentMethod().Name);
                            }
                            else
                            {
                                row.Cells["Week"].Value = row.Cells["Week"].Value.ToString() + " " + donestring;
                                row.ReadOnly = true;
                                Application.DoEvents();
                                hasmodifiedGarp = true;
                            }
                        }
                    }
                }

                if (hasmodifiedGarp)
                {
                    Thread.Sleep(1000);
                    lblInfo.Text = "Done saving to garp. Please wait...";
                    Application.DoEvents();

                    Thread.Sleep(1000);
                    lblInfo.Text = "Sorting orderrows in Garp. Please wait...";
                    Application.DoEvents();

                    m_Garphelper.SortModifiedOrderRowsInGarp();

                }
                moveFilesToProcessed();

                Thread.Sleep(1000);
                lblInfo.Text = "Done!";
                Application.DoEvents();
            }
            catch (Exception ex)
            {
                LoggMgr.WriteToLogg("ERR " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                lblInfo.Text = "";
            }
        }

        private bool isoutsidehorisontAndValid(DataGridViewRow row)
        {
            string deldate = row.Cells["Levtid"].Value.ToString();
            DateTime levDate = DateTime.ParseExact(deldate, "yyMMdd", CultureInfo.CurrentCulture);

            if (levDate > m_dateDaysAheadForecast.Date)
            {
                //it's a row outside horisont so check if it's goes through filter customer and article if any selected
                if (cmbArticle.Items.Count > 0 && cmbArticle.SelectedValue.ToString() != "0")
                {
                    //we have active filter on article so check row
                    if (cmbArticle.SelectedValue.ToString() != row.Cells["artNr"].Value.ToString())
                    {
                        return false;
                    }
                }

                if (cmbCustomer.Items.Count > 0 && cmbCustomer.SelectedValue.ToString() != "0")
                {
                    //we have active filter on customer so check row
                    if (cmbCustomer.SelectedValue.ToString() != row.Cells["customerNr"].Value.ToString())
                    {
                        return false;
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool isoutsidehorisont(DataGridViewRow row)
        {
            string deldate = row.Cells["Levtid"].Value.ToString();
            DateTime levDate = DateTime.ParseExact(deldate, "yyMMdd", CultureInfo.CurrentCulture);

            if (levDate > m_dateDaysAheadForecast.Date)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// If order is planned to be delivered erlier an today, today or tomorrow, it's cassified as present time
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        private bool isInPresentTime(DataGridViewRow row)
        {
            DateTime levDate = DateTime.ParseExact(row.Cells["Levtid"].Value.ToString(), "yyMMdd", CultureInfo.CurrentCulture);

            // If levdate is tomorrow or erlier, we are in present time
            if ((levDate.CompareTo(DateTime.Today.Date.AddDays(1).Date)) <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void moveFilesToProcessed()
        {
            string inFolder = Properties.Settings.Default.infolder;
            string outFolder = Properties.Settings.Default.outfolder;
            string inFilePrefix = Properties.Settings.Default.fileprefix;

            DirectoryInfo d = new DirectoryInfo(inFolder);
            FileInfo[] Files = d.GetFiles(inFilePrefix + "*");

            foreach (FileInfo file in Files)
            {
                string renamedfilesname = file.Name.Replace(file.Extension, DateTime.Now.ToString("yyMMddhhmmss") + file.Extension);
                file.MoveTo(outFolder + "\\" + file.Name);
            }
        }

        private void clearData()
        {
            dgvOrder.Rows.Clear();
            m_articleLevList.Clear();
        }

        private void populateCustomerFilter()
        {
            try
            {
                LoggMgr.debugIt("DEBUG " + System.Reflection.MethodBase.GetCurrentMethod().Name);
                doit = false;
                comboSourceCustomer.Clear();
                comboSourceCustomer.Add("0", "All...");

                foreach (string entry in DataHandler.CustomersFoundInFiles)
                {
                    comboSourceCustomer.Add(entry, entry);
                }

                cmbCustomer.DataSource = new BindingSource(comboSourceCustomer, null);
                cmbCustomer.DisplayMember = "Value";
                cmbCustomer.ValueMember = "Key";
                doit = true;

            }
            catch (Exception ex)
            {
                LoggMgr.WriteToLogg("ERR " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        private void populateProductFilter()
        {
            try
            {
                LoggMgr.debugIt("DEBUG " + System.Reflection.MethodBase.GetCurrentMethod().Name);
                doit = false;
                comboSourceProduct.Clear();
                comboSourceProduct.Add("0", "All...");

                string currCustomer = "0";

                if (cmbCustomer.Items.Count > 0)
                {
                    currCustomer = cmbCustomer.SelectedValue.ToString();
                }

                for (int i = 0; i < m_articleLevList.Count; i++)
                {
                    if (!comboSourceProduct.ContainsKey(m_articleLevList[i].Artnr_xml) && !string.IsNullOrEmpty(m_articleLevList[i].Artnr_xml))
                    {
                        if (currCustomer != "0")
                        {
                            //We have a customer filter active so check customer
                            if (currCustomer == m_articleLevList[i].Kundnr_garp)
                            {
                                comboSourceProduct.Add(m_articleLevList[i].Artnr_xml, m_articleLevList[i].Artnr_xml);
                            }
                        }
                        else
                        {
                            comboSourceProduct.Add(m_articleLevList[i].Artnr_xml, m_articleLevList[i].Artnr_xml);
                        }
                    }
                }

                cmbArticle.DataSource = new BindingSource(comboSourceProduct, null);
                cmbArticle.DisplayMember = "Value";
                cmbArticle.ValueMember = "Key";
                doit = true;
            }
            catch (Exception ex)
            {
                LoggMgr.WriteToLogg("ERR " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
            }

        }

        private void cmbCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (doit)
            {
                populateProductFilter();
                filterGrid();
            }

        }

        private void cmbArticle_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (doit)
            {
                filterGrid();
            }
        }


        private void filterGrid()
        {
            try
            {
                string filterArticle = "0";
                string filterCustomer = "0";

                if (cmbArticle.Items.Count > 0)
                {
                    filterArticle = cmbArticle.SelectedValue.ToString();
                }

                if (cmbCustomer.Items.Count > 0)
                {
                    filterCustomer = cmbCustomer.SelectedValue.ToString();
                }

                int numRowsGrid = 0;
                foreach (DataGridViewRow row in dgvOrder.Rows)
                {
              
                    if (filterArticle != "0" && filterCustomer != "0")
                    {
                        cmdSave.Enabled = false;

                        //article filter AND customer filter
                        if (filterArticle == row.Cells["artNr"].Value.ToString() && filterCustomer == row.Cells["customerNr"].Value.ToString())
                        {
                            row.Visible = true;
                            row.MinimumHeight = 32;
                            numRowsGrid++;
                        }
                        else
                        {
                            row.Visible = false;
                        }
                    }
                    //article filter ONLY
                    else if (filterArticle != "0" && filterCustomer == "0")
                    {
                        cmdSave.Enabled = false;
                        if (filterArticle == row.Cells["artNr"].Value.ToString())
                        {
                            row.Visible = true;
                            row.MinimumHeight = 32;
                            numRowsGrid++;
                        }
                        else
                        {
                            row.Visible = false;
                        }
                    }
                    //customer filter ONLY
                    else if (filterCustomer != "0" && filterArticle == "0")
                    {
                        cmdSave.Enabled = true;
                        if (filterCustomer == row.Cells["customerNr"].Value.ToString())
                        {
                            row.Visible = true;
                            row.MinimumHeight = 32;
                            numRowsGrid++;
                        }
                        else
                        {
                            row.Visible = false;
                        }
                    }
                    else
                    {
                        cmdSave.Enabled = true;
                        //no filter
                        row.Visible = true;
                        row.MinimumHeight = 32;
                        numRowsGrid++;
                    }
                }

                configGridRows();

            }
            catch (Exception ex)
            {
                LoggMgr.WriteToLogg("ERR " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
            }
        }


        //private void tableLayoutPanel_Paint(object sender, PaintEventArgs e)
        //{

        //}
    }
    class CustomDataGridView : DataGridView
    {
        public CustomDataGridView()
        {
            DoubleBuffered = true;
        }
    }
}
