﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarpOrderMgr
{
    public class ArticleDTO
    {
        public string Kundnr_garp { get; set; }
        public string Kundnr_xml { get; set; }
        public string Kundname_xml { get; set; }
        public string Artnr_xml { get; set; }
        public string Levtid_xml { get; set; }

        public string Ordernr { get; set; }
        public string Statustext { get; set; }
        public string Atgardtext { get; set; }
        public bool IsProductionPlanned { get; set; }

        public string Levtid_garp { get; set; }
        public string Radnr_garp { get; set; }


        public int Statustype { get; set; }
        public int Atgardtype { get; set; }
        public double Antal_xml { get; set; }
        public int Saldoefter { get; set; }
        public int Rowid_xml { get; set; }
        public string Week_xml { get; set; }

        public double Antal_garp { get; set; }

    }

    public class MatchOrderAndDeliverPlanResult
    {
        public List<ArticleDTO> MatchedList { get; set; }
        public List<OrderDTO> OrderWithCorrespondingProducts { get; set; }
    }
}
