﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarpOrderMgr
{
    public class CustomerDTO
    {
        public string AccountId { get; set; }
        public string Address3 { get; set; }
        public string Box { get; set; }
        public double CalculatedCreditTime { get; set; }
        public string CategoryDescription { get; set; }
        public string CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string ClaimCode { get; set; }
        public string Code_1 { get; set; }
        public string Code_2 { get; set; }
        public string Code_3 { get; set; }
        public string Code_4 { get; set; }
        public string Code_5 { get; set; }
        public string Code_6 { get; set; }
        public double ContributionMarginLastYear { get; set; }
        public double ContributionMarginThisYear { get; set; }
        public string CorporateIdentityNo { get; set; }
        public string CountryId { get; set; }
        public string CountryName { get; set; }
        public double CreditLimit { get; set; }
        public string CurrencyId { get; set; }
        public string CustomerNo { get; set; }
        public string CustomerNoFromFile { get; set; }
        public string CustomerType { get; set; }
        public string DeliverLockDescription { get; set; }
        public string DeliverLockId { get; set; }
        public string DeliverTermsId { get; set; }
        public string DeliverWay { get; set; }
        public string DeliverZip { get; set; }
        public string DiscountId { get; set; }
        public string DutyFree { get; set; }
        public string EANRecipientAddress { get; set; }
        public string EDIRecipientadress { get; set; }
        public string Fax { get; set; }
        public string GoodsZip { get; set; }
        public string InterestInvoice { get; set; }
        public string InvoiceCustomerName { get; set; }
        public string InvoiceCustomerNo { get; set; }
        public string LanguageCode1 { get; set; }
        public string LanguageCode2 { get; set; }
        public string LastInvoiceDate { get; set; }
        public string Name { get; set; }
        public double Numeric_1 { get; set; }
        public double Numeric_2 { get; set; }
        public double Numeric_3 { get; set; }
        public double Numeric_4 { get; set; }
        public double OpenAmount { get; set; }
        public string PaymentTermsId { get; set; }
        public string Phone { get; set; }
        public string PriceList1 { get; set; }
        public string PriceList2 { get; set; }
        public string PriceListId { get; set; }
        public string PriceListOrderType1 { get; set; }
        public string PriceListOrderType2 { get; set; }
        public string Reference { get; set; }
        public string SellerId { get; set; }
        public string SellerName { get; set; }
        public string Street { get; set; }
        public string Text { get; set; }
        public string Text_1 { get; set; }
        public string Text_10 { get; set; }
        public string Text_2 { get; set; }
        public string Text_3 { get; set; }
        public string Text_4 { get; set; }
        public string Text_5 { get; set; }
        public string Text_6 { get; set; }
        public string Text_7 { get; set; }
        public string Text_8 { get; set; }
        public string Text_9 { get; set; }
        public string TransportTime { get; set; }
        public double TurnoverLastYear { get; set; }
        public double TurnoverThisYear { get; set; }
        public string UnifiedInvoice { get; set; }
        public string VATId { get; set; }
        public string ZipCity { get; set; }
    }
}
