﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarpOrderMgr
{
    public class OrderDTO
    {
        public string AccountNo { get; set; }
        public bool AdditionalOrders { get; set; }
        public string CompanyId { get; set; }
        public string ContinueFromOrder { get; set; }
        public string ContinueOnOrder { get; set; }
        public string CostPlaceId { get; set; }
        public string CountryCode { get; set; }
        public string CurrencyId { get; set; }
        public string CurrencyValue { get; set; }
        public object CustomerInfo { get; set; }
        public string Date { get; set; }
        public string DeliverAddress1 { get; set; }
        public string DeliverAddress2 { get; set; }
        public string DeliverAddress3 { get; set; }
        public object DeliverCity { get; set; }
        public DeliverCustomerDTO DeliverCustomer { get; set; }
        public object DeliverCustomerCode { get; set; }
        public object DeliverCustomerLockId { get; set; }
        public string DeliverCustomerName { get; set; }
        public string DeliverCustomerNo { get; set; }
        public object DeliverCustomerPhone { get; set; }
        public object DeliverCustomerType { get; set; }
        public string DeliverPath { get; set; }
        public string DeliverPlace { get; set; }
        public string DeliverState { get; set; }
        public string DeliverTerms { get; set; }
        public object DeliverTermsDescription { get; set; }
        public string DeliverWay { get; set; }
        public object DeliverWayDescription { get; set; }
        public object DeliverZip { get; set; }
        public string DeliverZipCity { get; set; }
        public string DeliveredValue { get; set; }
        public string DiscountId { get; set; }
        public string DiscountPercentage { get; set; }
        public string ExtraId { get; set; }
        public object InvoiceCustomerAddress1 { get; set; }
        public object InvoiceCustomerAddress2 { get; set; }
        public object InvoiceCustomerAddress3 { get; set; }
        public object InvoiceCustomerCity { get; set; }
        public object InvoiceCustomerCountryCode { get; set; }
        public object InvoiceCustomerName { get; set; }
        public string InvoiceCustomerNo { get; set; }
        public object InvoiceCustomerVATId { get; set; }
        public object InvoiceCustomerZipCode { get; set; }
        public bool IsDeliveryAddressUnique { get; set; }
        public string LastDeliverDate { get; set; }
        public object LastDeliverNote { get; set; }
        public object LastDeliverNoteState { get; set; }
        public string NotDeliveredValue { get; set; }
        public string OrderConfirmationState { get; set; }
        public string OrderNo { get; set; }
        public List<object> OrderRowZeroTexts { get; set; }
        public List<OrderRowDTO> OrderRows { get; set; }
        public string OrderType { get; set; }
        public string Order_ttmm { get; set; }
        public string OurReferenceId { get; set; }
        public string OurReferenceName { get; set; }
        public string PaymentTerms { get; set; }
        public string PickListState { get; set; }
        public string PreferedDeliverDate { get; set; }
        public string PriceListId { get; set; }
        public string ProjectId { get; set; }
        public object ProjectName { get; set; }
        public string ReservedId { get; set; }
        public string Season { get; set; }
        public SellerDTO Seller { get; set; }
        public string SellerId { get; set; }
        public object SellerName { get; set; }
        public object ShipmentNo { get; set; }
        public string Stopped { get; set; }
        public string Text1 { get; set; }
        public string Text2 { get; set; }
        public string Text3 { get; set; }
        public string Text4 { get; set; }
        public double TotalVolyme { get; set; }
        public double TotalWeight { get; set; }
        public string TransportTime { get; set; }
        public string VatId { get; set; }
        public string X1F { get; set; }
        public string YoureReference { get; set; }
    }


    public class DeliverCustomerDTO
    {
        public string AccountId { get; set; }
        public string Address3 { get; set; }
        public string Box { get; set; }
        public int CalculatedCreditTime { get; set; }
        public string CategoryDescription { get; set; }
        public string CategoryId { get; set; }
        public string ClaimCode { get; set; }
        public string Code_1 { get; set; }
        public string Code_2 { get; set; }
        public string Code_3 { get; set; }
        public string Code_4 { get; set; }
        public string Code_5 { get; set; }
        public string Code_6 { get; set; }
        public double ContributionMarginLastYear { get; set; }
        public double ContributionMarginThisYear { get; set; }
        public string CorporateIdentityNo { get; set; }
        public string CountryId { get; set; }
        public int CreditLimit { get; set; }
        public string CurrencyId { get; set; }
        public string CustomerNo { get; set; }
        public string CustomerType { get; set; }
        public object DeliverLockDescription { get; set; }
        public string DeliverLockId { get; set; }
        public string DeliverTermsId { get; set; }
        public string DeliverWay { get; set; }
        public object DeliverZip { get; set; }
        public string DiscountId { get; set; }
        public string DutyFree { get; set; }
        public string EDIRecipientadress { get; set; }
        public string Fax { get; set; }
        public string GoodsZip { get; set; }
        public string InterestInvoice { get; set; }
        public object InvoiceCustomerName { get; set; }
        public string InvoiceCustomerNo { get; set; }
        public string LanguageCode1 { get; set; }
        public string LanguageCode2 { get; set; }
        public string LastInvoiceDate { get; set; }
        public string Name { get; set; }
        public int Numeric_1 { get; set; }
        public int Numeric_2 { get; set; }
        public int Numeric_3 { get; set; }
        public int Numeric_4 { get; set; }
        public double OpenAmount { get; set; }
        public string PaymentTermsId { get; set; }
        public string Phone { get; set; }
        public string PriceList1 { get; set; }
        public string PriceList2 { get; set; }
        public string PriceListId { get; set; }
        public string PriceListOrderType1 { get; set; }
        public string PriceListOrderType2 { get; set; }
        public string Reference { get; set; }
        public string SellerId { get; set; }
        public string Street { get; set; }
        public object Text { get; set; }
        public string Text_1 { get; set; }
        public string Text_10 { get; set; }
        public string Text_2 { get; set; }
        public string Text_3 { get; set; }
        public string Text_4 { get; set; }
        public string Text_5 { get; set; }
        public string Text_6 { get; set; }
        public string Text_7 { get; set; }
        public string Text_8 { get; set; }
        public string Text_9 { get; set; }
        public string TransportTime { get; set; }
        public double TurnoverLastYear { get; set; }
        public double TurnoverThisYear { get; set; }
        public string UnifiedInvoice { get; set; }
        public string VATId { get; set; }
        public string ZipCity { get; set; }
    }

    public class ProductTextDTO
    {
        public string Code { get; set; }
        public string Field { get; set; }
        public string Value { get; set; }
    }

    public class WarehouseListDTO
    {
        public string Description { get; set; }
        public string Description2 { get; set; }
        public string Id { get; set; }
        public string OrderPoint { get; set; }
        public double OrderQuantity { get; set; }
        public string ProductNo { get; set; }
        public double QuantityInOrder { get; set; }
        public double QuantityInPurchase { get; set; }
        public double Stock { get; set; }
        public string VIP { get; set; }
    }

    public class ProductDTO
    {
        public int AmountDecimalCount { get; set; }
        public int AmountPerUnit { get; set; }
        public double AverageCostPrice { get; set; }
        public string Category { get; set; }
        public string Code1 { get; set; }
        public string Code2 { get; set; }
        public string Code3 { get; set; }
        public string Code4 { get; set; }
        public string Code5 { get; set; }
        public string Code6 { get; set; }
        public string CompanyId { get; set; }
        public string CountryOfOrigin { get; set; }
        public string DeliveryCode { get; set; }
        public string Description { get; set; }
        public object Dimension { get; set; }
        public string DiscountPurchaseCode { get; set; }
        public string DiscountSalesCode { get; set; }
        public int ExtraPrice1 { get; set; }
        public int ExtraPrice2 { get; set; }
        public string HVL { get; set; }
        public int Id { get; set; }
        public string LeadTime { get; set; }
        public string MVL { get; set; }
        public string MaterialAccount { get; set; }
        public string OrderPoint { get; set; }
        public string OrderQuantity { get; set; }
        public string OriginType { get; set; }
        public double Price { get; set; }
        public int PriceDecimalCount { get; set; }
        public object PriceInStore { get; set; }
        public string PriceModelCode { get; set; }
        public string ProductGroup1 { get; set; }
        public string ProductGroup2 { get; set; }
        public string ProductGroup3 { get; set; }
        public string ProductGroup4 { get; set; }
        public string ProductGroup5 { get; set; }
        public string ProductNo { get; set; }
        public string ProductRange1 { get; set; }
        public string ProductRange2 { get; set; }
        public List<ProductTextDTO> ProductText { get; set; }
        public string ProductType { get; set; }
        public string PurchaseAccount { get; set; }
        public string PurchaseCurrencyCode { get; set; }
        public double PurchasePrice { get; set; }
        public string SaleableCode { get; set; }
        public object SalesAccount { get; set; }
        public string SalesAccountId { get; set; }
        public string Season { get; set; }
        public double StandardPrice { get; set; }
        public double Stock { get; set; }
        public string StockUpdateType { get; set; }
        public string SupplierNo { get; set; }
        public string SuppliersProductNo { get; set; }
        public string Unit { get; set; }
        public string VATCode { get; set; }
        public object Variant { get; set; }
        public string VariantCode { get; set; }
        public double Volyme { get; set; }
        public string WOB { get; set; }
        public List<WarehouseListDTO> WarehouseList { get; set; }
        public string WarehouseNo { get; set; }
        public string WarehouseType { get; set; }
        public double Weight { get; set; }
    }

    public class OrderRowDTO
    {
        public string AccountId { get; set; }
        public double Amount { get; set; }
        public int AmountDecimalCount { get; set; }
        public string AmountState { get; set; }
        public double AmountToDeliver { get; set; }
        public string BDF { get; set; }
        public string BLF { get; set; }
        public object BRA { get; set; }
        public string CostPlaceId { get; set; }
        public double CostPrice { get; set; }
        public double DIM { get; set; }
        public string DeliverState { get; set; }
        public double DeliveredAmount { get; set; }
        public double DiscardAmount { get; set; }
        public string DiscountId { get; set; }
        public string DiscountPercentage { get; set; }
        public string ExtraId { get; set; }
        public string FixedCostPrice { get; set; }
        public double GrossPrice { get; set; }
        public object HRF { get; set; }
        public string KTR { get; set; }
        public string MDF { get; set; }
        public string MKT { get; set; }
        public string MTF { get; set; }
        public double NX1 { get; set; }
        public string OrderConfirmationState { get; set; }
        public string OrderNo { get; set; }
        public string Origin { get; set; }
        public string PickListState { get; set; }
        public string PreferedDeliverDate { get; set; }
        public double Price { get; set; }
        public int PriceDecimalCount { get; set; }
        public ProductDTO Product { get; set; }
        public string ProductDescription { get; set; }
        public string ProductDescription2 { get; set; }
        public string ProductNo { get; set; }
        public string ProjectId { get; set; }
        public string PurchaseState { get; set; }
        public string ReservedId { get; set; }
        public string RowNo { get; set; }
        public string RowType { get; set; }
        public double RowValue { get; set; }
        public string StatisticState { get; set; }
        public List<object> Textrows { get; set; }
        public string Unit { get; set; }
        public bool UseOHDiscount { get; set; }
        public string VatId { get; set; }
        public string WarehouseNo { get; set; }
        public string X1F { get; set; }
        public string X2F { get; set; }
    }

    public class SellerDTO
    {
        public string CostPlace { get; set; }
        public string EmployeNo { get; set; }
        public string ExtName { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string UserId { get; set; }
    }



    public class OrderRowSimpleDTO
    {
        public string OrderNo { get; set; }
        public string OrderType { get; set; }
        public string DeliverCustomerNo { get; set; }
        public string DeliverState { get; set; }
        public string BLF { get; set; }
    }



    public class OrderForAddDTO
    {
        public string OrderNo { get; set; }
        public string DeliverCustomerNo { get; set; }
        public string Text1 { get; set; }
        public string OurReferenceId { get; set; }
        public List<OrderRowForAddDTO> OrderRows { get; set; }
        public string OrderType { get; set; }
        public bool IsDeliveryAddressUnique { get; set; }
    }

    
    public class OrderRowForAddDTO
    {
        public string OrderNo { get; set; }
        public string ProductNo { get; set; }
        public double Amount { get; set; }
        public string RowNo { get; set; }
        public string PreferedDeliverDate { get; set; }
        //public string BLF { get; set; }
    }



}
