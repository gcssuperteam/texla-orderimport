﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GarpOrderMgr
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 

        public const string g_ACTION_NEWORDER = "New order";
        public const string g_ACTION_NEWORDERROW = "New orderrow";
        public const string g_ACTION_NEWQUANTITY = "New quantity";
        public const string g_ACTION_DELETE = "Delete";

        public const int g_ACTION_NEWORDER_NUM = 0;
        public const int g_ACTION_NEWORDERROW_NUM = 1;
        public const int g_ACTION_NEWQUANTITY_NUM = 2;
        public const int g_ACTION_DELETE_NUM = 3;


        public const string g_STATUS_LEVREADY = "Ready for delivery";
        public const string g_STATUS_PLANNED = "Planned";
        public const string g_STATUS_NOTPLANNED = "Not Prod planned";
        public const string g_STATUS_PROGNOS = "Forecast";
        public const string g_STATUS_NOT_PROGNOS = "None Forecast";
        public const string g_STATUS_CUST_NOT_FOUND = "Customer not found";
        public const string g_STATUS_ART_NOT_FOUND = "Article not found";


        public const int g_STATUS_LEVREADY_NUM = 0;
        public const int g_STATUS_PLANNED_NUM = 1;
        public const int g_STATUS_NOTPLANNED_NUM = 2;
        public const int g_STATUS_PROGNOS_NUM = 3;
        public const int g_STATUS_CUST_NOT_FOUND_NUM = 4;
        public const int g_STATUS_ART_NOT_FOUND_NUM = 5;
        public const int g_STATUS_NOT_PROGNOS_NUM = 6;





        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Mainform());
        }




    }
}
