﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GarpOrderMgr.Classes;
using Newtonsoft.Json;

namespace GarpOrderMgr
{
    class GarpMgr
    {
        string m_GarpBolag = Properties.Settings.Default.garpbolag;
        string m_GarpUser = Properties.Settings.Default.garpuser;
        string m_GarpPwd = Properties.Settings.Default.garppwd;

        string m_GISurlOrder = Properties.Settings.Default.gisurlOrder;
        string m_GISurlCustomer = Properties.Settings.Default.gisurlCustomer;
        string m_GISurlProduct = Properties.Settings.Default.gisurlProduct; 

        string m_GIStoken = Properties.Settings.Default.gistoken;

        string m_newOrderNoSerie = Properties.Settings.Default.newOrderNo;
        string m_OrderNoSerieForecast = Properties.Settings.Default.newOrderNoForecast;
        string m_OrderTypeForcast = Properties.Settings.Default.OrderTypeForcast;
        string m_newOrderTypeNormal = Properties.Settings.Default.newOrderTypeNormal;
        string m_newOrderOurRef = Properties.Settings.Default.newOrderOurRef;
        List<string> m_custList = new List<string>();

        //public List<OrderDTO> m_GisOrderObj = null;
        List<OrderForAddDTO> m_newGisOrderObj = null;
        List<OrderForAddDTO> m_newGisOrderObjPlanned = null;

        List<string> m_modifiedOrderNoList = null;

        public void init()
        {
            m_newGisOrderObj = new List<OrderForAddDTO>();
            m_newGisOrderObjPlanned = new List<OrderForAddDTO>();
            m_modifiedOrderNoList = new List<string>();
        }

        public string getCustNameByCustNoGIS(string custno)
        {

            return "";
        }


        public List<CustomerDTO> getGISCustomers()
        {
            string token = m_GIStoken;
            string jsonString = "";

            string indexfilter = "*";
            string generalfilter = "*";

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(m_GISurlCustomer);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //get all rows for this articleno in Garp (filter on levtid)
            HttpResponseMessage response = client.GetAsync("GetCustomerList/" + token + "/1/" + indexfilter + "/" + generalfilter).Result;

            if (response.IsSuccessStatusCode)
            {
                jsonString = response.Content.ReadAsStringAsync().Result;
                List<CustomerDTO> GisCustObjList = null;
                try
                {
                    GisCustObjList = JsonConvert.DeserializeObject<List<CustomerDTO>>(jsonString);
                    if (GisCustObjList != null && GisCustObjList.Count > 0)
                    {
                        return GisCustObjList;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception e)
                {
                    LoggMgr.WriteToLogg("problem deserializeObject CustomerList " + System.Reflection.MethodBase.GetCurrentMethod().Name, e);
                    return null;
                }
            }
            else
            {
                LoggMgr.WriteToLogg("bad response in webrequest to GIS " + System.Reflection.MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }


        //public CustomerDTO getCustByCustNoGIS(string custno)
        //{
        //    string token = m_GIStoken;
        //    string jsonString = "";

        //    HttpClient client = new HttpClient();
        //    client.BaseAddress = new Uri(m_GISurlCustomer);
        //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //    //get all rows for this articleno in Garp (filter on levtid)
        //    HttpResponseMessage response = client.GetAsync("GetCustomer/" + token + "/" + custno).Result;

        //    if (response.IsSuccessStatusCode)
        //    {
        //        jsonString = response.Content.ReadAsStringAsync().Result;
        //        if (jsonString.IndexOf(@"""CustomerNo""" + ":null") > 0)
        //        {
        //            //Customer not found
        //            return null;
        //        }

        //        CustomerDTO GisCustObj = null;
        //        try
        //        {
        //            GisCustObj = JsonConvert.DeserializeObject<CustomerDTO>(jsonString);
        //            if (GisCustObj != null)
        //            {
        //                return GisCustObj;
        //            }
        //            else
        //            {
        //                return null;
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            LoggMgr.WriteToLogg("problem deserializeObject Customer (" + custno + ") " + System.Reflection.MethodBase.GetCurrentMethod().Name, e);
        //            return null;
        //        }
        //    }
        //    else
        //    {
        //        LoggMgr.WriteToLogg("bad response in webrequest to GIS (" + custno + ") " + System.Reflection.MethodBase.GetCurrentMethod().Name);
        //        return null;
        //    }

        //}


        //public List<CustomerDTO> getCustByEANGIS(string custno)
        //{
        //    ///GetCustomerList/{token}/{index}/{indexfilter}/{general_filter_string}
        //    string token = m_GIStoken;
        //    string jsonString = "";

        //    string indexfilter = "*";
        //    string generalfilter = "TX3==xxxxx";

        //    //generalfilter = "EAM==562666";

        //    HttpClient client = new HttpClient();
        //    client.BaseAddress = new Uri(m_GISurlCustomer);
        //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //    //get all rows for this articleno in Garp (filter on levtid)
        //    HttpResponseMessage response = client.GetAsync("GetCustomerList/" + token + "/1/" + indexfilter + "/" + generalfilter).Result;

        //    if (response.IsSuccessStatusCode)
        //    {
        //        jsonString = response.Content.ReadAsStringAsync().Result;
        //        List<CustomerDTO> GisCustObjList = null;
        //        try
        //        {
        //            GisCustObjList = JsonConvert.DeserializeObject<List<CustomerDTO>>(jsonString);
        //            if (GisCustObjList != null && GisCustObjList.Count > 0)
        //            {
        //                return GisCustObjList;
        //            }
        //            else
        //            {
        //                return null;
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            LoggMgr.WriteToLogg("problem deserializeObject CustomerList " + System.Reflection.MethodBase.GetCurrentMethod().Name, e);
        //            return null;
        //        }
        //    }
        //    else
        //    {
        //        LoggMgr.WriteToLogg("bad response in webrequest to GIS " + System.Reflection.MethodBase.GetCurrentMethod().Name);
        //        return null;
        //    }

        //}

        public List<ProductDTO> getGISArticles()
        {
            string token = m_GIStoken;
            string jsonString = "";

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(m_GISurlProduct);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //get all rows products with filter
            HttpResponseMessage response = client.GetAsync("GetProductList/" + token + "/1/*/*").Result;

            if (response.IsSuccessStatusCode)
            {
                jsonString = response.Content.ReadAsStringAsync().Result;
                List<ProductDTO> GisProductObjList = null;
                try
                {
                    GisProductObjList = JsonConvert.DeserializeObject<List<ProductDTO>>(jsonString);
                    if (GisProductObjList != null && GisProductObjList.Count > 0)
                    {
                        return GisProductObjList;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception e)
                {
                    LoggMgr.WriteToLogg("problem deserializeObject ProductList " + System.Reflection.MethodBase.GetCurrentMethod().Name, e);
                    return null;
                }
            }
            else
            {
                LoggMgr.WriteToLogg("bad response in webrequest to GIS " + System.Reflection.MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }

        private OrderDTO GetGisOrderByOnr(string onr)
        {
            try
            {
                LoggMgr.debugIt("DEBUG " + System.Reflection.MethodBase.GetCurrentMethod().Name);
                string jsonString = "";
                string token = m_GIStoken;

                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(m_GISurlOrder);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = client.GetAsync("GetOrderById/" + token + "/" + onr + "/true").Result;

                if (response.IsSuccessStatusCode)
                {
                    jsonString = response.Content.ReadAsStringAsync().Result;

                    try
                    {
                        OrderDTO tmpGisOrderObj = JsonConvert.DeserializeObject<OrderDTO>(jsonString);
                        return tmpGisOrderObj;
                    }
                    catch (Exception e)
                    {
                        LoggMgr.WriteToLogg("problem deserializeObject GisOrder(" + onr + ") " + System.Reflection.MethodBase.GetCurrentMethod().Name, e);
                        return null;
                    }
                }
                else
                {
                    LoggMgr.WriteToLogg("bad response in webrequest to GIS (" + onr + ") " + System.Reflection.MethodBase.GetCurrentMethod().Name);
                    return null;
                }
            }
            catch (Exception e)
            {
                LoggMgr.WriteToLogg("problem in " + System.Reflection.MethodBase.GetCurrentMethod().Name, e);
                return null;
            }
            finally
            {

            }
        }

        private List<OrderDTO> fillCustomersOrder(string kundnr)
        {
            List<OrderDTO> result = new List<OrderDTO>();

            try
            {
                LoggMgr.debugIt("DEBUG " + System.Reflection.MethodBase.GetCurrentMethod().Name);
                string token = m_GIStoken;
                string filter = "LEF!=5;OTY!=0";

                try
                {
                    if (!string.IsNullOrEmpty(kundnr))
                    {
                        WebRequest request = WebRequest.Create(m_GISurlOrder + "GetOrderByIdxList/" + token + "/2/" + kundnr + "/" + filter + "/true/0/false");
                        request.Timeout = 3600000;

                        LoggMgr.debugIt("Order request: " + request.RequestUri);
                        WebResponse ws = request.GetResponse();
                        Encoding enc = System.Text.Encoding.GetEncoding(1252);
                        StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                        string response = responseStream.ReadToEnd();
                        responseStream.Close();


                        result = JsonConvert.DeserializeObject<List<OrderDTO>>(response);
                    }
                    else
                    {
                        LoggMgr.debugIt("");
                    }

                }
                catch (Exception e)
                {
                    LoggMgr.WriteToLogg("problem deserializeObject GisOrder(" + kundnr + ") " + System.Reflection.MethodBase.GetCurrentMethod().Name, e);
                }
            }
            catch (Exception e)
            {
                LoggMgr.WriteToLogg("problem in " + System.Reflection.MethodBase.GetCurrentMethod().Name, e);
            }
            finally
            {

            }

            return result;
        }

        /// <summary>
        /// Add Garp data to each entry of the articleLevList
        /// </summary>
        /// <param name="articleLevList"></param>
        /// <param name="infoLabel"></param>
        /// <param name="m_kundNr"></param>
        /// <returns></returns>
        public List<ArticleDTO> addGarpOrderData(List<ArticleDTO> articleLevList, Label infoLabel, string m_kundNr)
        {
            List<ArticleDTO> result = new List<ArticleDTO>();
            List<OrderDTO> allOrderWithHandeledProducts = new List<OrderDTO>();

            try
            {
                LoggMgr.debugIt("DEBUG " + System.Reflection.MethodBase.GetCurrentMethod().Name);
                if (!m_custList.Contains(m_kundNr))
                {
                    m_custList.Add(m_kundNr);
                }

                // Check if we got anny deliverplans, we filter on customer before this funcion and i can be zero hits on that filter, no need to continou if so..
                if(articleLevList?.Count != 0)
                {
                    MatchOrderAndDeliverPlanResult matchResult = getAllOrderRowsOnMathingDeliverPlan(articleLevList, infoLabel, m_kundNr);
                    result.AddRange(matchResult.MatchedList);

                    result.AddRange(addCustOrderRowForDeleteOption(matchResult.OrderWithCorrespondingProducts, articleLevList, infoLabel, m_kundNr));
                }
            }
            catch (Exception e)
            {
                LoggMgr.WriteToLogg("problem in " + System.Reflection.MethodBase.GetCurrentMethod().Name, e);
            }
            finally
            {

            }

            return result;
        }

        /// <summary>
        /// Gets all orderrows that in some way match DeliverPlan. Also returns all readed order on all product in the DelivePlan list
        /// </summary>
        /// <param name="articleLevList"></param>
        /// <param name="infoLabel"></param>
        /// <param name="m_kundNr"></param>
        /// <returns></returns>
        private MatchOrderAndDeliverPlanResult getAllOrderRowsOnMathingDeliverPlan(List<ArticleDTO> articleLevList, Label infoLabel, string m_kundNr)
        {
            MatchOrderAndDeliverPlanResult result = new MatchOrderAndDeliverPlanResult();
            result.MatchedList = new List<ArticleDTO>();
            result.OrderWithCorrespondingProducts = new List<OrderDTO>();
            
            //load all customers order which are not delivered (LEF<=4)
            List<OrderDTO> orderList = fillCustomersOrder(m_kundNr);

            // Filter out all ArticleDTO with right customer and that exists in garp
            var articleList = articleLevList.Where(a => a.Kundnr_garp == m_kundNr && a.Statustext != Program.g_STATUS_ART_NOT_FOUND && a.Statustext != Program.g_STATUS_CUST_NOT_FOUND)?.ToList();
            int idx = 0;

            foreach (ArticleDTO articlePlan in articleLevList)
            {
                infoLabel.Text = "Loading filecontent and garpdata, please wait... (" + (idx++).ToString() + ")";
                Application.DoEvents();
                List<OrderRowSimpleDTO> possibleOnrForAnrCandidates = new List<OrderRowSimpleDTO>();
                ArticleDTO article = null;

                // Full match (product, quantity and deliverdate) och ANTAL ej noll?
                var orderFullMatch = orderList.Where(o => o.OrderRows.Any(r => r.ProductNo == articlePlan.Artnr_xml && r.Amount == articlePlan.Antal_xml && r.PreferedDeliverDate == articlePlan.Levtid_xml))?.ToList();

                // Partial match (product, deliverdate) och ANTAL ej noll
                var orderDeliverTimeMatch = orderList.Where(o => o.OrderRows.Any(r => r.ProductNo == articlePlan.Artnr_xml && r.PreferedDeliverDate == articlePlan.Levtid_xml))?.ToList();

                // Only match on productno
                var orderProducOnlytMatch = orderList.Where(o => o.OrderRows.Any(r => r.ProductNo == articlePlan.Artnr_xml))?.ToList();

                if (orderFullMatch?.Count > 0)
                {
                    OrderRowDTO orderRowGarp = orderFullMatch[0].OrderRows.Where(r => r.ProductNo == articlePlan.Artnr_xml && r.Amount == articlePlan.Antal_xml && r.PreferedDeliverDate == articlePlan.Levtid_xml)?.First();

                    article = checkOrderRows(orderRowGarp, articlePlan, orderFullMatch[0].OrderType);

                    // If found we break the loop
                    if (article != null)
                    {
                        article.Atgardtext = "";
                        article.Atgardtype = -1;
                    }
                }
                else if (orderDeliverTimeMatch?.Count > 0)
                {
                    OrderRowDTO orderRowGarp = orderDeliverTimeMatch[0].OrderRows.Where(r => r.ProductNo == articlePlan.Artnr_xml && r.PreferedDeliverDate == articlePlan.Levtid_xml)?.First();
                    article = checkOrderRows(orderRowGarp, articlePlan, orderDeliverTimeMatch[0].OrderType);
                }
                else
                {
                    //Do we have any order to add this article to
                    if (orderProducOnlytMatch?.Count > 0 && articlePlan.Antal_xml > 0)
                    {
                        bool foundForecast = false;

                        foreach (OrderDTO orderNoMatch in orderProducOnlytMatch)
                        {
                            foreach (OrderRowDTO orderRowGarpSimple in orderNoMatch.OrderRows)
                            {
                                //try to find a forecastrow
                                if (orderNoMatch.OrderType == m_OrderTypeForcast)
                                {
                                    article = articlePlan;
                                    //Forecast
                                    article.Atgardtext = Program.g_ACTION_NEWORDERROW;
                                    article.Atgardtype = Program.g_ACTION_NEWORDERROW_NUM;

                                    article.Ordernr = orderRowGarpSimple.OrderNo;
                                    article.Statustext = Program.g_STATUS_PROGNOS;
                                    article.Statustype = Program.g_STATUS_PROGNOS_NUM;
                                    foundForecast = true;
                                    break;
                                }
                            }
                        }
                        if (!foundForecast)
                        {
                            //ingen orderrad hittad för aktuell artikel om antal är större än noll
                            //ingen forecast order bland de vi samlade in ovan för aktuell artikel
                            article = articlePlan;
                            article.Atgardtext = Program.g_ACTION_NEWORDER;
                            article.Atgardtype = Program.g_ACTION_NEWORDER_NUM;
                        }
                    }
                    else
                    {
                        //ingen orderrad hittad för aktuell artikel om antal är större än noll
                        if(articlePlan.Antal_xml > 0)
                        {
                            article = articlePlan;
                            article.Atgardtext = Program.g_ACTION_NEWORDER;
                            article.Atgardtype = Program.g_ACTION_NEWORDER_NUM;
                        }
                    }
                }


                if (article != null)
                {
                    result.MatchedList.Add(article);
                }


            }

            var productList = articleLevList.GroupBy(a => a.Artnr_xml).Select(n=> new ArticleDTO { Artnr_xml = n.Key });
            foreach (ArticleDTO prodno in productList)
            {
                result.OrderWithCorrespondingProducts.AddRange(orderList.Where(o => o.OrderRows.Any(r => r.ProductNo == prodno.Artnr_xml))?.ToList());
            }

            return result;
        }

        /// <summary>
        /// Get orderows that match by productno, deliverdate, quantity. Either
        /// * Same ProductNo && same PreferedDeliverDate && same quantity
        /// * Same ProductNo && same PreferedDeliverDate && NOT same quantity
        /// </summary>
        /// <param name="orderGarp"></param>
        /// <param name="article"></param>
        /// <returns></returns>
        private ArticleDTO checkOrderRows(OrderRowDTO orderRowGarp, ArticleDTO article, string orderType)
        {
            ArticleDTO result = null;

            if (orderRowGarp.ProductNo != "" && Convert.ToInt32(orderRowGarp.Amount) > 0)
            {
                // Set proper status on orderrow
                SetRowStatusText(orderRowGarp.BLF, orderRowGarp.DeliverState, article, orderType);
                
                // If quantity is zero we change this to a DELETE instead of NEW_QUANTITY
                if(article.Antal_xml == 0)
                {
                    article.Antal_garp = orderRowGarp.Amount;
                    article.Radnr_garp = orderRowGarp.RowNo.ToString();
                    article.Ordernr = orderRowGarp.OrderNo;

                    article.Atgardtext = Program.g_ACTION_DELETE;
                    article.Atgardtype = Program.g_ACTION_DELETE_NUM;

                    result = article;
                }
                //same anr AND levtime AND same quantity
                else if (article.Artnr_xml == orderRowGarp.ProductNo && article.Levtid_xml == orderRowGarp.PreferedDeliverDate && article.Antal_xml == orderRowGarp.Amount)
                {
                    article.Antal_garp = orderRowGarp.Amount;
                    article.Radnr_garp = orderRowGarp.RowNo.ToString();
                    article.Ordernr = orderRowGarp.OrderNo;
                    //article.Saldoefter = Convert.ToInt32(orderRowGarp.Product.Stock) - Convert.ToInt32(orderRowGarp.Amount);

                    result = article;
                }
                //same anr AND same levtime AND NOT same quantity
                else if (article.Artnr_xml == orderRowGarp.ProductNo && article.Levtid_xml == orderRowGarp.PreferedDeliverDate && article.Antal_xml != orderRowGarp.Amount)
                {
                    article.Antal_garp = orderRowGarp.Amount;
                    article.Radnr_garp = orderRowGarp.RowNo.ToString();
                    article.Ordernr = orderRowGarp.OrderNo;
                    //article.Saldoefter = Convert.ToInt32(orderRowGarp.Product.Stock) - Convert.ToInt32(orderRowGarp.Amount);

                    if (article.Statustype != Program.g_STATUS_LEVREADY_NUM && article.Statustype != Program.g_STATUS_PLANNED_NUM)
                    {
                        string plusOrMinus = "+";
                        if (article.Antal_xml < orderRowGarp.Amount)
                        {
                            plusOrMinus = "-";
                        }

                        article.Atgardtext = Program.g_ACTION_NEWQUANTITY + "(" + plusOrMinus + Math.Round(Math.Abs(article.Antal_xml - orderRowGarp.Amount), 1) + ")";
                        article.Atgardtype = Program.g_ACTION_NEWQUANTITY_NUM;
                    }

                    result = article;
                }
            }

            return result;
        }

         private bool SetRowStatusText(string BLF, string deliverstate, ArticleDTO articleLevList, string orderType)
        {
            if (orderType == m_OrderTypeForcast)
            {
                //Forecast
                articleLevList.Statustext = Program.g_STATUS_PROGNOS;
                articleLevList.Statustype = Program.g_STATUS_PROGNOS_NUM;
            }
            else if (orderType != "0" && Convert.ToInt32(BLF) == 0 && Convert.ToInt32(deliverstate) < 5)
            {
                //Not planned
                articleLevList.Statustext = Program.g_STATUS_NOTPLANNED;
                articleLevList.Statustype = Program.g_STATUS_NOTPLANNED_NUM;
            }
            else if (orderType != "0" && Convert.ToInt32(BLF) == 1 && Convert.ToInt32(deliverstate) < 5)
            {
                //Planned
                articleLevList.Statustext = Program.g_STATUS_PLANNED;
                articleLevList.Statustype = Program.g_STATUS_PLANNED_NUM;
            }
            else if (orderType != "0" && Convert.ToInt32(BLF) >= 4 && Convert.ToInt32(deliverstate) < 5)
            {
                //Ready for delivery
                articleLevList.Statustext = Program.g_STATUS_LEVREADY;
                articleLevList.Statustype = Program.g_STATUS_LEVREADY_NUM;
            }

            return true;

        }

        private List<ArticleDTO> addCustOrderRowForDeleteOption(List<OrderDTO> orderList, List<ArticleDTO> articleLevList, Label infoLabel, string m_kundNr)
        {
            List<ArticleDTO> result = new List<ArticleDTO>();

            try
            {
                int ordercount = 0;
                List<simpleOrderRow> custOrderNoMatchForDelete = new List<simpleOrderRow>();

                //loop all not delivered orders for this customer
                foreach (OrderDTO orderGarp in orderList)
                {
                    infoLabel.Text = "Loading garpdata for delete option, please wait... (" + (ordercount + 1).ToString() + ")";
                    Application.DoEvents();
                    //loop all orderrows for this order

                    foreach (OrderRowDTO orderRowGarp in orderGarp.OrderRows)
                    {

                        // Convert.ToInt32(orderRowGarp.BLF) == 0 &&
                        if (orderRowGarp.ProductNo != "" && Convert.ToInt32(orderRowGarp.DeliverState) < 4)
                        {
                            bool foundAnr = false;
                            bool foundAnrAndLevdate = false;
                            //check if anr exist but none with same levdate as this custom orderrow
                            for (int i = 0; i < articleLevList.Count; i++)
                            {
                                //m_GisOrderObj can contain many different customers order
                                //only check this row if we are on correct customerorder
                                if (orderGarp.DeliverCustomerNo == articleLevList[i].Kundnr_garp && articleLevList[i].Kundnr_garp == m_kundNr) {
                                    if (articleLevList[i].Artnr_xml == orderRowGarp.ProductNo)
                                    {
                                        //customer has an order with this article
                                        foundAnr = true;

                                        if(articleLevList[i].Levtid_xml == orderRowGarp.PreferedDeliverDate)
                                        {
                                            foundAnrAndLevdate = true;
                                            break;
                                        }
                                    }
                                }
                            }

                            if (foundAnr && !foundAnrAndLevdate)
                            {
                                int daysAhead = Properties.Settings.Default.daysAhead;
                                DateTime dateDaysAhead = DateTime.Now.AddDays(daysAhead);
                                bool levDateOK = true;

                                if (orderRowGarp.PreferedDeliverDate != "" && orderRowGarp.PreferedDeliverDate.Length == 6)
                                {
                                    DateTime levDate = DateTime.ParseExact(orderRowGarp.PreferedDeliverDate, "yyMMdd", CultureInfo.CurrentCulture);
                                    levDateOK = levDate <= dateDaysAhead;
                                }
                                else if(orderRowGarp.PreferedDeliverDate.Length != 6)
                                {
                                    levDateOK = false;
                                }
                                else
                                {
                                    levDateOK = true;
                                }
                                
                                //check date is withing days ahead limit
                                if (levDateOK)
                                {
                                    //customer has an order with this article but none with the requested levtime
                                    //add CustomerRow to temp list structure for delete option
                                    simpleOrderRow mySimpleOrderRow = new simpleOrderRow();
                                    mySimpleOrderRow.OrderNo = orderRowGarp.OrderNo;
                                    mySimpleOrderRow.ProductNo = orderRowGarp.ProductNo;
                                    mySimpleOrderRow.RowNo = orderRowGarp.RowNo;
                                    mySimpleOrderRow.PreferedDeliverDate = orderRowGarp.PreferedDeliverDate;
                                    mySimpleOrderRow.Amount = Convert.ToInt32(orderRowGarp.Amount);
                                    mySimpleOrderRow.DD_CustomerNo = orderGarp.DeliverCustomerNo;
                                    mySimpleOrderRow.OrderType = orderGarp.OrderType;
                                    mySimpleOrderRow.DD_CustomerName = orderGarp.DeliverCustomerName;
                                    mySimpleOrderRow.BLF = orderRowGarp.BLF;
                                    mySimpleOrderRow.DeliverState = orderRowGarp.DeliverState;

                                    custOrderNoMatchForDelete.Add(mySimpleOrderRow);
                                }


                            }
                        }

                    }//loop customers orderrows
                    ordercount++;
                }//loop customers order

                if(custOrderNoMatchForDelete.Count > 0)
                {
                    //add CustomerRow to articleLevList for delete option
                    foreach (simpleOrderRow simpleorderRow in custOrderNoMatchForDelete)
                    {
                        ArticleDTO artLev = new ArticleDTO();
                        artLev.Rowid_xml = articleLevList.Count + 1;
                        artLev.Kundnr_garp = simpleorderRow.DD_CustomerNo;
                        artLev.Kundname_xml = simpleorderRow.DD_CustomerName;
                        artLev.Artnr_xml = simpleorderRow.ProductNo;
                        artLev.Antal_xml = 0;

                        SetRowStatusText(simpleorderRow.BLF, simpleorderRow.DeliverState, artLev, simpleorderRow.OrderType);

                        artLev.Levtid_xml = simpleorderRow.PreferedDeliverDate;

                        if(artLev.Levtid_xml != "")
                        {
                            DateTime levDate = DateTime.ParseExact(artLev.Levtid_xml, "yyMMdd", CultureInfo.CurrentCulture);                     
                            int thisweek = FileParser.GetWeekNumber(levDate);
                            string strWeek = "";

                            if (thisweek < 10)
                            {
                                strWeek = levDate.Year.ToString().Substring(2, 2) + "0" + thisweek.ToString();
                            }
                            else
                            {
                                strWeek = levDate.Year.ToString().Substring(2, 2) + thisweek.ToString();
                            }
                            artLev.Week_xml = strWeek;
                        }
                        else
                        {
                            artLev.Week_xml = "-1";
                        }



                        artLev.Levtid_garp = simpleorderRow.PreferedDeliverDate;
                        artLev.Antal_garp = simpleorderRow.Amount;
                        artLev.Saldoefter = -1;
                        artLev.Ordernr = simpleorderRow.OrderNo;
                        artLev.Radnr_garp = simpleorderRow.RowNo;

                        //if(simpleorderRow.OrderType == m_OrderTypeForcast)
                        //{
                        //    artLev.Statustext = Program.g_STATUS_PROGNOS;
                        //    artLev.Statustype = Program.g_STATUS_PROGNOS_NUM;
                        //}
                        //else
                        //{
                        //    artLev.Statustext = "";
                        //    artLev.Statustype = -1;
                        //}

                        artLev.Atgardtext = Program.g_ACTION_DELETE;
                        artLev.Atgardtype = Program.g_ACTION_DELETE_NUM;

                        result.Add(artLev);
                    }
                }

            }
            catch (Exception e)
            {
                LoggMgr.WriteToLogg("problem in " + System.Reflection.MethodBase.GetCurrentMethod().Name, e);
            }
            finally
            {

            }

            return result;
        }

        public bool ProcessDataGridViewRow(DataGridViewRow row, List<ArticleDTO> articleLevList)
        {
            //Här hamnar vi om Change ELLER Plan är ikryssat
            if (row.Cells["Atgard"].Value.ToString() == Program.g_ACTION_NEWORDER)
            {
                if (row.Cells["Plan"].Value.ToString() == "1")
                {
                    addPlannedOrder(row, articleLevList, m_newOrderNoSerie);
                    //addForecastOrder(row, articleLevList, m_newOrderNoSerie);
                }
                else
                {
                    addForecastOrder(row, articleLevList, m_OrderNoSerieForecast);
                }
            }
            else if (row.Cells["Atgard"].Value.ToString() == Program.g_ACTION_NEWORDERROW)
            {
                if (row.Cells["Plan"].Value.ToString() == "1")
                {
                    //it should be planned so add a new order instead of new orderrow
                    addPlannedOrder(row, articleLevList, m_newOrderNoSerie);
                    //addForecastOrder(row, articleLevList, m_newOrderNoSerie);
                }
                else
                {
                    int idxArtLevList = getIdxArtLevListForGridRow(row, articleLevList);
                    ArticleDTO thisartlevrow = articleLevList[idxArtLevList];

                    if (thisartlevrow.Statustext != Program.g_STATUS_PROGNOS)
                    {
                        //it is a new orderrow which is not a forecast so create new forecast order
                        addForecastOrder(row, articleLevList, m_newOrderNoSerie);
                    }
                    else
                    {
                        addOrderRow(row, articleLevList, null);
                        //store changed orderNo so we later can sort the rows for it i Garp
                        storeModifiedOnrs(row.Cells["OrderNo"].Value.ToString());
                    }

                }
            }
            else if (row.Cells["Atgard"].Value.ToString().IndexOf(Program.g_ACTION_NEWQUANTITY) != -1)
            {
                if (row.Cells["Plan"].Value.ToString() == "1")
                {
                    //it should be planned so delete AND add a new order instead
                    deleteOrderRow(row, articleLevList);
                    addPlannedOrder(row, articleLevList, m_newOrderNoSerie);
                    //addForecastOrder(row, articleLevList, m_newOrderNoSerie);

                    //store changed orderNo so we later can sort the rows for it i Garp
                    storeModifiedOnrs(row.Cells["OrderNo"].Value.ToString());
                }
                else
                {
                    updateOrderRowAmount(row, articleLevList);
                }
            }
            else if (row.Cells["Atgard"].Value.ToString() == Program.g_ACTION_DELETE)
            {
                //delete row in garp
                deleteOrderRow(row, articleLevList);

                if (row.Cells["Plan"].Value.ToString() == "1")
                {
                    addPlannedOrder(row, articleLevList, m_newOrderNoSerie);
                    //addForecastOrder(row, articleLevList, m_newOrderNoSerie);
                }

                storeModifiedOnrs(row.Cells["OrderNo"].Value.ToString());
            }
            else if ((row.Cells["Atgard"].Value.ToString().IndexOf("Normal order") != -1 || row.Cells["Atgard"].Value.ToString() == "") && row.Cells["Status"].Value.ToString() == Program.g_STATUS_PROGNOS)
            {
                //planering av en forecast rad
                //delete row in garp
                deleteOrderRow(row, articleLevList);

                if (row.Cells["Plan"].Value.ToString() == "1")
                {
                    addPlannedOrder(row, articleLevList, m_newOrderNoSerie);
                    //addForecastOrder(row, articleLevList, m_newOrderNoSerie);
                }

                storeModifiedOnrs(row.Cells["OrderNo"].Value.ToString());
            }
            return true;
        }



        //Sort orderrows in Garp that we have touched so they are in ascending levdate
        public bool SortModifiedOrderRowsInGarp()
        {
            try
            {
                if (m_modifiedOrderNoList != null && m_modifiedOrderNoList.Count > 0)
                {
                    string token = m_GIStoken;

                    foreach(string onr in m_modifiedOrderNoList)
                    {
                        OrderDTO myGisOrder = GetGisOrderByOnr(onr);

                        //It seems that we need to delete the rows before adding them later, if not it did for some reason add one row
                        foreach (OrderRowDTO orderRowGarp in myGisOrder.OrderRows)
                        {
                            try
                            {
                                updateMess response = GISCaller.ExecutePost<OrderRowDTO, updateMess>(m_GISurlOrder + @"DeleteOrderRow/", orderRowGarp);


                                try
                                {
                                    if (response.Succeeded?.ToLower() == "false")
                                    {
                                        LoggMgr.WriteToLogg(response.Message, null);
                                        return false;
                                    }
                                }
                                catch(Exception e)
                                {
                                    LoggMgr.debugIt("Error logging failed (SortModifiedOrderRowsInGarp) : " + e.Message);
                                }
                            }
                            catch (Exception e)
                            {
                                LoggMgr.debugIt("Error in SortModifiedOrderRowsInGarp: " + e.Message);
                            }
 
                        }//orderrow loop

                        //we have an order which we might have modified so loop all orderrows for this order
                        sortOrderRowsOnLevdate(myGisOrder.OrderRows);

                        //nu ligger dom i rätt ordning men med fel rowno så loopa igen om ändra detta
                        int rowNoCount = 1;
                        foreach (OrderRowDTO orderRowGarp in myGisOrder.OrderRows)
                        {
                            orderRowGarp.RowNo = rowNoCount.ToString();
                            rowNoCount++;
                        }

                        try
                        {
                            try
                            {
                                //send sorted orderrow to Garp
                                updateMess response = GISCaller.ExecutePost<List<OrderRowDTO>, updateMess>(m_GISurlOrder + @"UpdateOrderRowList/", myGisOrder.OrderRows);

                                if (response.Succeeded?.ToLower() == "true")
                                {
                                    LoggMgr.debugIt("ERR " + "UpdateOrderRowList, Could not sort orderrow for order:  " + myGisOrder.OrderNo + " " + System.Reflection.MethodBase.GetCurrentMethod().Name);
                                    return false;
                                }
                                else
                                {
                                    LoggMgr.debugIt("Call failed: " + response.ErrorMessage);
                                }
                            }
                            catch (Exception e)
                            {
                                LoggMgr.debugIt("Error in SortModifiedOrderRowsInGarp: " + e.Message);
                            }

                        }
                        catch (Exception e)
                        {
                            LoggMgr.debugIt("Error in SortModifiedOrderRowsInGarp - " + e.Message);
                        }
                    }
                }

                return true;
            }

            catch (Exception e)
            {
                LoggMgr.WriteToLogg("ERR " + System.Reflection.MethodBase.GetCurrentMethod().Name, e);
                return false;
            }
        }

        //ascending articlenum, descending levdate_xml
        private bool sortOrderRowsOnLevdate(List <OrderRowDTO> myorderRows)
        {
            try
            {
                myorderRows.Sort(delegate (OrderRowDTO p1, OrderRowDTO p2)
                {
                    //alla med amount 0 should be at the bottom

                   if (Convert.ToInt32(p1.Amount) == 0 && Convert.ToInt32(p2.Amount) == 0)
                    {
                        //they are equal
                        return 0;
                    }
                    else if (Convert.ToInt32(p1.Amount) > 0 && Convert.ToInt32(p2.Amount) == 0)
                    {
                        //p1 is greater
                        return -1;
                    }
                    else if (Convert.ToInt32(p1.Amount) == 0 && Convert.ToInt32(p2.Amount) > 0)
                    {
                        //p2 is greater
                        return 1;
                    }

                    return p1.PreferedDeliverDate.CompareTo(p2.PreferedDeliverDate);
                });

                return true;
            }
            catch (Exception e)
            {
                LoggMgr.WriteToLogg("ERR " + System.Reflection.MethodBase.GetCurrentMethod().Name, e);
                return false;
            }
        }

        private void storeModifiedOnrs(string onr)
        {
            //remove rownum info if exist
            if(onr.IndexOf("-")!= -1)
            {
                onr = onr.Split('-')[0];
            }
          
            if (!m_modifiedOrderNoList.Contains(onr))
            {
                m_modifiedOrderNoList.Add(onr);
            }       
        }



        private bool addForecastOrder(DataGridViewRow row, List<ArticleDTO> articleLevList, string onrSerie)
        {
            try
            {
                //Find the index for this gridrow in our list of objects
                int idxArtLevList = getIdxArtLevListForGridRow(row, articleLevList);

                if (idxArtLevList != -1)
                {
                    ArticleDTO thisartlevrow = articleLevList[idxArtLevList];

                    //First check if it has been added earlier when looping through the grid
                    //Is plan checked?
                    if (row.Cells["Plan"].Value.ToString() == "1")
                    {
                        if (m_newGisOrderObjPlanned != null)
                        {
                            OrderForAddDTO myOrder = getNewlyAddedOnrByAnrPlanned(thisartlevrow.Artnr_xml);
                            if (myOrder != null && myOrder.DeliverCustomerNo == thisartlevrow.Kundnr_garp)
                            {
                                return addOrderRow(row, articleLevList, myOrder);
                            }
                        }
                    }
                    else
                    {
                        if (m_newGisOrderObj != null)
                        {
                            OrderForAddDTO myOrder = getNewlyAddedOnrByAnr(thisartlevrow.Artnr_xml);
                            if (myOrder != null && myOrder.DeliverCustomerNo == thisartlevrow.Kundnr_garp)
                            {
                                return addOrderRow(row, articleLevList, myOrder);
                            }
                        }
                    }

                    OrderForAddDTO myNewGisOrder = new OrderForAddDTO();
                    myNewGisOrder.DeliverCustomerNo = thisartlevrow.Kundnr_garp;
                    myNewGisOrder.OrderNo = onrSerie;
                    myNewGisOrder.IsDeliveryAddressUnique = false;

                    myNewGisOrder.Text1 = thisartlevrow.Kundnr_xml;
                    myNewGisOrder.OurReferenceId = m_newOrderOurRef;

                    OrderRowForAddDTO newOrderRow = new OrderRowForAddDTO();
                    //newOrderRow.OrderNo = thisartlevrow.Ordernr;
                    newOrderRow.RowNo = "255";
                    newOrderRow.ProductNo = thisartlevrow.Artnr_xml;
                    newOrderRow.PreferedDeliverDate = thisartlevrow.Levtid_xml;

                    //Om vi har en delete som skall planeras så behåller vi det antalet som fanns i garp istället för 0
                    if (row.Cells["Atgard"].Value.ToString() == Program.g_ACTION_DELETE)
                    {
                        newOrderRow.Amount = thisartlevrow.Antal_garp;
                    }
                    else
                    {
                        newOrderRow.Amount = thisartlevrow.Antal_xml;
                    }
                    

                    //Is plan checked?
                    if (row.Cells["Plan"].Value.ToString() == "1")
                    {
                        myNewGisOrder.OrderType = m_newOrderTypeNormal;

                        //mail från Nylander 181008 13:54 Du kan ta bort den. Den skall inte sättas alls av programmet.
                        //newOrderRow.BLF = "1";
                    }
                    else
                    {
                        myNewGisOrder.OrderType = m_OrderTypeForcast;
                    }

                    myNewGisOrder.OrderRows = new List<OrderRowForAddDTO>();
                    myNewGisOrder.OrderRows.Add(newOrderRow);

                    string token = m_GIStoken;

                    try
                    {
                        string response = GISCaller.ExecutePost<OrderForAddDTO, string>(m_GISurlOrder + @"AddOrderHead/", myNewGisOrder);
                        string resOnr = response?.Replace("\"", "").Replace(@"\", "");

                        //Add the new order to our local collection of new orders
                        if (row.Cells["Plan"].Value.ToString() == "1")
                        {
                            myNewGisOrder.OrderNo = resOnr;
                            m_newGisOrderObjPlanned.Add(myNewGisOrder);
                        }
                        else
                        {
                            myNewGisOrder.OrderNo = resOnr;
                            m_newGisOrderObj.Add(myNewGisOrder);
                        }

                        return true;
                    }
                    catch (Exception e)
                    {
                        LoggMgr.debugIt("Error in addForecastOrder: " + e.Message);
                    }

                }
                return true;
            }
            catch (Exception e)
            {
                LoggMgr.WriteToLogg("Problem with " + row.Cells["ID"].Value.ToString() + " " + System.Reflection.MethodBase.GetCurrentMethod().Name, e);
                return false;
            }
        }

        private bool addPlannedOrder(DataGridViewRow row, List<ArticleDTO> articleLevList, string onrSerie)
        {
            try
            {
                //Find the index for this gridrow in our list of objects
                int idxArtLevList = getIdxArtLevListForGridRow(row, articleLevList);

                if (idxArtLevList != -1)
                {
                    ArticleDTO thisartlevrow = articleLevList[idxArtLevList];

                    OrderForAddDTO myNewGisOrder = new OrderForAddDTO();
                    myNewGisOrder.DeliverCustomerNo = thisartlevrow.Kundnr_garp;
                    myNewGisOrder.OrderNo = onrSerie;
                    myNewGisOrder.IsDeliveryAddressUnique = false;
                    myNewGisOrder.OrderType = m_newOrderTypeNormal;

                    myNewGisOrder.Text1 = thisartlevrow.Kundnr_xml;
                    myNewGisOrder.OurReferenceId = m_newOrderOurRef;

                    OrderRowForAddDTO newOrderRow = new OrderRowForAddDTO();
                    //newOrderRow.OrderNo = thisartlevrow.Ordernr;
                    newOrderRow.RowNo = "255";
                    newOrderRow.ProductNo = thisartlevrow.Artnr_xml;
                    newOrderRow.PreferedDeliverDate = thisartlevrow.Levtid_xml;

                    //Om vi har en delete som skall planeras så behåller vi det antalet som fanns i garp istället för 0
                    if (row.Cells["Atgard"].Value.ToString() == Program.g_ACTION_DELETE)
                    {
                        newOrderRow.Amount = thisartlevrow.Antal_garp;
                    }
                    else
                    {
                        newOrderRow.Amount = thisartlevrow.Antal_xml;
                    }

                    myNewGisOrder.OrderRows = new List<OrderRowForAddDTO>();
                    myNewGisOrder.OrderRows.Add(newOrderRow);

                    string token = m_GIStoken;

                    try
                    {
                        //HttpResponseMessage responseOrder = client.PostAsJsonAsync<OrderForAddDTO>("AddOrderHead/" + token, myNewGisOrder).Result;
                        string response = GISCaller.ExecutePost<OrderForAddDTO, string>(m_GISurlOrder + @"AddOrderHead/", myNewGisOrder);
                        string resOnr = response?.Replace("\"", "").Replace(@"\", "");

                        //Add the new order to our local collection of new orders
                        if (row.Cells["Plan"].Value.ToString() == "1")
                        {
                            myNewGisOrder.OrderNo = resOnr;
                            m_newGisOrderObjPlanned.Add(myNewGisOrder);
                        }
                        else
                        {
                            myNewGisOrder.OrderNo = resOnr;
                            m_newGisOrderObj.Add(myNewGisOrder);
                        }

                        return true;
                    }
                    catch (Exception e)
                    {
                        LoggMgr.debugIt("Error in addPlannedOrder: " + e.Message);
                    }
 
                }
                return true;
            }
            catch (Exception e)
            {
                LoggMgr.WriteToLogg("Problem with " + row.Cells["ID"].Value.ToString() + " " + System.Reflection.MethodBase.GetCurrentMethod().Name, e);
                return false;
            }
        }

        private bool addOrderRow(DataGridViewRow row, List<ArticleDTO> articleLevList, OrderForAddDTO myGisOrder)
        {
            try
            {
                //Find the index for this gridrow in our list of objects
                int idxArtLevList = getIdxArtLevListForGridRow(row, articleLevList);

                if (idxArtLevList != -1)
                {
                    ArticleDTO thisartlevrow = articleLevList[idxArtLevList];
                    string token = m_GIStoken;

                    HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri(m_GISurlOrder);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    string onr = "";

                    if (myGisOrder != null)
                    {
                        //får in en GisOrder ifall vi nyligen lagt till en ny order från datagriden
                        onr = myGisOrder.OrderNo;
                    }
                    else
                    {
                        //Hämta aktuell orderno
                        onr = thisartlevrow.Ordernr;
                    }
          
                    List<OrderRowDTO> newOrderRowList = new List<OrderRowDTO>();

                    OrderRowDTO newOrderRow = new OrderRowDTO();
                    newOrderRow.OrderNo = onr;
                    newOrderRow.RowNo = "255";
                    newOrderRow.ProductNo = thisartlevrow.Artnr_xml;
                    newOrderRow.PreferedDeliverDate = thisartlevrow.Levtid_xml;
                    newOrderRow.Amount = thisartlevrow.Antal_xml;
                    newOrderRowList.Add(newOrderRow);

                    //Fick lov att bygga en enklare orderrow som endast innehåller nödvändiga fält. Om jag skickar med price fältet som blir fallet i ovan orderrow så slår ej Garps priskalkylering in
                    List<OrderRowForAddDTO> newOrderRowForAddList = new List<OrderRowForAddDTO>();

                    OrderRowForAddDTO newOrderRowForAdd = new OrderRowForAddDTO();
                    newOrderRowForAdd.OrderNo = onr;
                    newOrderRowForAdd.RowNo = "255";              
                    newOrderRowForAdd.ProductNo = thisartlevrow.Artnr_xml;
                    newOrderRowForAdd.PreferedDeliverDate = thisartlevrow.Levtid_xml;
                    newOrderRowForAdd.Amount = thisartlevrow.Antal_xml;
                    newOrderRowForAddList.Add(newOrderRowForAdd);

                    LoggMgr.debugIt("Sending request: " + m_GISurlOrder + "UpdateOrderRowList/" + token);
                    LoggMgr.debugIt("Sending request body: " + JsonConvert.SerializeObject(newOrderRowForAddList));

                    try
                    {
                        //HttpResponseMessage responseOrder = client.PostAsJsonAsync<List<OrderRowForAddDTO>>("UpdateOrderRowList/" + token, newOrderRowForAddList).Result;
                        GISCaller.ExecutePost<List<OrderRowForAddDTO>, object>(m_GISurlOrder + @"UpdateOrderRowList/", newOrderRowForAddList);
                        return true;
                    }
                    catch (Exception e)
                    {
                        LoggMgr.debugIt("Error in addOrderRow: " + e.Message);
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                LoggMgr.WriteToLogg("Problem with " + row.Cells["ID"].Value.ToString() + " " +  System.Reflection.MethodBase.GetCurrentMethod().Name, e);
                return false;
            }
        }


        private bool updateOrderRowAmount(DataGridViewRow row, List<ArticleDTO> articleLevList)
        {
            try
            {
                int idxArtLevList = getIdxArtLevListForGridRow(row, articleLevList);

                if (idxArtLevList != -1)
                {
                    ArticleDTO thisartlevrow = articleLevList[idxArtLevList];
                    string token = m_GIStoken;

                    //Hämta aktuell orderrad från våran tidigare hämtade kollektion av order för kunden
                    OrderRowDTO thisGisOrderRow = GetOrderRow(thisartlevrow);
                    if (thisGisOrderRow != null)
                    {
                        //if plan is checked we need to set amount to 0 for atleast "New quantity" rows, "Delete" rows has already amount 0
                        //this is a procedure we only do temporarily because we have no GIS metod for deleting orderrows yet
                        thisGisOrderRow.Amount = thisartlevrow.Antal_xml;

                        try
                        {
                            updateMess response = GISCaller.ExecutePost<OrderRowDTO, updateMess> (m_GISurlOrder + @"UpdateOrderRow/", thisGisOrderRow);

                            if (response.Succeeded.ToLower() == "false")
                            {
                                LoggMgr.WriteToLogg(response.Message, null);
                                return false;
                            }
                            else
                            {
                                //Everything went fine
                                return true;
                            }
                        }
                        catch (Exception e)
                        {
                            LoggMgr.debugIt("Error in updateOrderRowAmount: " + e.Message);
                            return false;
                        }
 
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                LoggMgr.WriteToLogg("Problem with " + row.Cells["ID"].Value.ToString() + " " + System.Reflection.MethodBase.GetCurrentMethod().Name, e);
                return false;
            }
        }



        private bool deleteOrderRow(DataGridViewRow row, List<ArticleDTO> articleLevList)
        {
            try
            {
                int idxArtLevList = getIdxArtLevListForGridRow(row, articleLevList);

                if (idxArtLevList != -1)
                {
                    ArticleDTO thisartlevrow = articleLevList[idxArtLevList];
                    string token = m_GIStoken;

                    //Hämta aktuell orderrad från våran tidigare hämtade kollektion av order för kunden
                    OrderRowDTO thisGisOrderRow = GetOrderRow(thisartlevrow);
                    if (thisGisOrderRow != null)
                    {
                        try
                        {
                            updateMess response = GISCaller.ExecutePost<OrderRowDTO, updateMess>(m_GISurlOrder + @"DeleteOrderRow/", thisGisOrderRow);

                            string updatemess = response.Result;
                            updateMess updatemessObj = JsonConvert.DeserializeObject<updateMess>(updatemess);

                            if (updatemessObj.Succeeded.ToLower() == "false")
                            {
                                LoggMgr.WriteToLogg(updatemessObj.Message, null);
                                return false;
                            }
                            else
                            {
                                //Everything went fine
                                return true;
                            }
                        }
                        catch (Exception e)
                        {
                            LoggMgr.debugIt("Error in deleteOrderRow: " + e.Message);
                            return false;
                        }
 
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                LoggMgr.WriteToLogg("Problem with " + row.Cells["ID"].Value.ToString() + " " + System.Reflection.MethodBase.GetCurrentMethod().Name, e);
                return false;
            }
        }




        //private OrderDTO GetOrder(ArticleDTO thisartlevrow)
        //{
        //    try
        //    {
        //        //loop all orders for this customer
        //        foreach (OrderDTO orderGarp in m_GisOrderObj)
        //        {
        //            if (thisartlevrow.Ordernr == orderGarp.OrderNo)
        //            {
        //                return orderGarp;
        //            }
        //        }

        //        return null;
        //    }
        //    catch (Exception e)
        //    {
        //        LoggMgr.WriteToLogg("Problem in" + System.Reflection.MethodBase.GetCurrentMethod().Name, e);
        //        return null;
        //    }
        //}

 
        private OrderRowDTO GetOrderRow(ArticleDTO thisartlevrow)
        {
            try
            {
                OrderRowDTO orderrow = GISCaller.GetOrderRow(thisartlevrow.Ordernr, thisartlevrow.Radnr_garp);
                return orderrow;
            }
            catch (Exception e)
            {
                LoggMgr.WriteToLogg("Problem in" + System.Reflection.MethodBase.GetCurrentMethod().Name, e);
                return null;
            }
        }


        private int getIdxArtLevListForGridRow(DataGridViewRow row, List<ArticleDTO> articleLevList)
        {
            for (int i = 0; i < articleLevList.Count; i++)
            {
                
                if (articleLevList[i].Rowid_xml == Convert.ToInt32(row.Cells["ID"].Value))
                {
                    return i;
                }
            }
            return -1;
        }


        private OrderForAddDTO getNewlyAddedOnrByAnr(string anr)
        {
            foreach (OrderForAddDTO orderGarp in m_newGisOrderObj)
            {
                foreach (OrderRowForAddDTO orderRowGarp in orderGarp.OrderRows)
                {
                    if (orderRowGarp.ProductNo == anr)
                    {
                        //found the row
                        return orderGarp;
                    }
                }
            }
            return null;
        }

        private OrderForAddDTO getNewlyAddedOnrByAnrPlanned(string anr)
        {
            foreach (OrderForAddDTO orderGarp in m_newGisOrderObjPlanned)
            {
                foreach (OrderRowForAddDTO orderRowGarp in orderGarp.OrderRows)
                {
                    if (orderRowGarp.ProductNo == anr)
                    {
                        //found the row
                        return orderGarp;
                    }
                }
            }
            return null;
        }


        public void clearNewlyAddedOrders()
        {
            try
            {
                if (m_newGisOrderObj != null)
                {
                    m_newGisOrderObj.Clear();
                    m_newGisOrderObj = null;
                }
                if (m_newGisOrderObjPlanned != null)
                {
                    m_newGisOrderObjPlanned.Clear();
                    m_newGisOrderObjPlanned = null;
                }
            }
            catch { }

        }

        public void clearStoredModifiedOnrs()
        {
            try
            {
                if (m_modifiedOrderNoList != null)
                {
                    m_modifiedOrderNoList.Clear();
                    m_modifiedOrderNoList = null;
                }
            }
            catch { }
        }

        public class updateMess
        {
            public string ErrorMessage { get; set; }
            public string Message { get; set; }
            public string Result { get; set; }
            public string Succeeded { get; set; }
        }


        public class simpleOrderRow
        {
            public string OrderNo { get; set; }
            public string RowNo { get; set; }
            public string ProductNo { get; set; }
            public string PreferedDeliverDate { get; set; }
            public double Amount { get; set; }
            public string DD_CustomerNo { get; set; }
            public string DD_CustomerName { get; set; }
            public string OrderType { get; set; }
            public string DeliverState { get; set; }
            public string BLF { get; set; } // ProductionPlannedState
        }

        public class newOrderInfo
        {
            public string OrderNo { get; set; }
            public string ProductNo { get; set; }
        }

    }
}
