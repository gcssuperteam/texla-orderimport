﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GarpOrderMgr.Classes
{
    public static class GISCaller
    {
        public static R ExecutePost<T,R>(string address, T body)
        {
            R result = default(R);
            string m_GIStoken = Properties.Settings.Default.gistoken;

            try
            {
                var request = WebRequest.Create(address + m_GIStoken);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                LoggMgr.debugIt("Sending request: " + address + m_GIStoken);
                LoggMgr.debugIt("Sending request body: " + JsonConvert.SerializeObject(body));

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(body));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string json = reader.ReadToEnd();
                    result = JsonConvert.DeserializeObject<R>(json);
                }
            }
            catch (Exception e)
            {
                LoggMgr.WriteToLogg("Error in ExecutePost", e);
            }

            return result;
        }

        public static OrderRowDTO GetOrderRow(string orderid, string rowid)
        {
            string rest = "";
            OrderRowDTO result = null;
            string m_GIStoken = Properties.Settings.Default.gistoken;
            string m_GISurlOrder = Properties.Settings.Default.gisurlOrder;

            try
            {
                if (!string.IsNullOrEmpty(orderid) && !string.IsNullOrEmpty(rowid))
                {
                    rest = m_GISurlOrder + "/REST/GetOrderRow" + "/" + m_GIStoken +  orderid + "/" + rowid;

                    LoggMgr.debugIt("Sending request: " + rest + m_GIStoken);

                    WebRequest request = WebRequest.Create(rest);
                    WebResponse ws = request.GetResponse();
                    Encoding enc = System.Text.Encoding.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    result = JsonConvert.DeserializeObject<OrderRowDTO>(response);
                }
            }
            catch (Exception e)
            {
                LoggMgr.WriteToLogg("Error in ExecutePost", e);
            }

            return result;
        }

    }
}
