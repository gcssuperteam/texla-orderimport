﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GarpOrderMgr
{
    public static class LoggMgr
    {
        public static void WriteToLogg(string TextToWrite, Exception ex = null)
        {
            try
            {
                //to loggfile
                if (Properties.Settings.Default.logglevel == 1)
                {
                    WriteToFile(TextToWrite, ex);

                }

                //to screen & loggfile
                else if (Properties.Settings.Default.logglevel == 2)
                {
                    if (ex != null)
                    {
                        MessageBox.Show(TextToWrite + '\n' + ex.Message, "Error message");
                    }
                    else
                    {
                        MessageBox.Show(TextToWrite, "Error message");
                    }

                    WriteToFile(TextToWrite, ex);
                }
            }

            catch (Exception exa)
            {
                string res = exa.Message;
            }
        }


        private static void WriteToFile(string TextToWrite, Exception ex = null)
        {
            try
            {
                if (System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\GarpOrderMgrLogg.txt"))
                {
                    FileInfo fileDetail = new System.IO.FileInfo(System.AppDomain.CurrentDomain.BaseDirectory + @"\GarpOrderMgrLogg.txt");

                    // remove loggfile if bigger than 10mb
                    if (fileDetail.Length >= 10000000)
                        System.IO.File.Delete(System.AppDomain.CurrentDomain.BaseDirectory + @"\GarpOrderMgrLogg.txt");
                }

                /* TODO ERROR: Skipped IfDirectiveTrivia *//* TODO ERROR: Skipped DisabledTextTrivia *//* TODO ERROR: Skipped EndIfDirectiveTrivia */
                using (StreamWriter logg = new StreamWriter(System.AppDomain.CurrentDomain.BaseDirectory + @"\GarpOrderMgrLogg.txt", true))
                {
                    logg.WriteLine(DateTime.Now + "  " + TextToWrite);
                    if (ex != null)
                        logg.WriteLine(">" + ex.Message);
                    logg.Close();
                }
            }
            catch (Exception exa)
            {
                string res = exa.Message;
            }
        }



        public static void debugIt(string TextToWrite)
        {
            try
            {
                if (Properties.Settings.Default.debug == true)
                {
                    WriteToFile(TextToWrite, null);
                }


            }
            catch (Exception e)
            {

            }
        }


    }
}
