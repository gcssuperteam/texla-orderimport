﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarpOrderMgr
{
    public static class DataHandler
    {
        public static List<CustomerDTO> GIS_Customers { get; set; }
        public static List<ProductDTO> GIS_Products { get; set; }
        public static List<string> CustomersFoundInFiles { get; set; }
        public static List<ProductInFile> ProductsFoundInFiles { get; set; }
        //public static Dictionary<string, string> CustomersFoundInFiles { get; set; }
        //public static Dictionary<string, string> ProductsFoundInFiles { get; set; }

        static DataHandler()
        {
            GIS_Customers = new List<CustomerDTO>();
            GIS_Products = new List<ProductDTO>();
            CustomersFoundInFiles = new List<string>();
            ProductsFoundInFiles = new List<ProductInFile>();
        }

        public static CustomerDTO getCustomerFromGISList(string custno)
        {
            foreach (CustomerDTO cust in GIS_Customers)
            {
                if (custno == cust.CustomerNo)
                {
                    return cust;
                }
                //Sample code, todo change text1
                else if (custno == cust.EANRecipientAddress)
                {
                    return cust;
                }
            }
            return null;
        }

        public static ProductDTO getProductFromGISList(string prodNo)
        {
            ProductDTO result = null;

            try
            {
                result = GIS_Products.Find(p => p.ProductNo == prodNo);

                if(result == null)
                {
                    foreach (ProductDTO p in GIS_Products)
                    {
                        //if (p.ProductNo.Equals("1126436"))
                        //{
                        //    Console.Write("");
                        //}
                        if (p.ProductText != null)
                        {
                            if (p.ProductText[7].Value.Trim() == prodNo.Trim())
                            {
                                result = p;
                                break;
                            }
                        }
                    }
                }
            }
            catch(Exception e)
            {

            }

            return result;
        }
    }

    public class ProductInFile
    {
        public string ProductNo { get; set; }
        public string File { get; set; }
    }

}
