﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarpOrderMgr
{
    public static class FileParser
    {
//        private static LoggMgr m_Logger = new LoggMgr();

        // Local holders to customer an d product mapped to each found segmentid in the current file. This is preloaded before we read the records from the file
        // and are necessary because customer/product id can befound on deifferent places or in records that are after detailsrow, so we preload them with the "segmentid" as key.
        //
        // OBS! Do not mix this with DataHandler.CustomersFoundInFiles and DataHandler.ProductsFoundInFiles that holds ALL customer an ALL products found in ALL files
        //
        private static List<SegmentKey> mProductList= new List<SegmentKey>();
        private static List<SegmentKey> mCustomerList = new List<SegmentKey>();

        private static List<string> mTotalRowsAllFiles = new List<string>();

        static FileParser()
        {
        }

        //read in the file to our object structure
        //infile can be either version 1 or 2
        //version 1: HEAD is before DEL
        //version 2: DEL is before HEAD
        public static List<ArticleDTO> CreateArticlesFromFile(string infile)
        {
            List<ArticleDTO> result = new List<ArticleDTO>();

            try
            {
                LoggMgr.debugIt("DEBUG " + System.Reflection.MethodBase.GetCurrentMethod().Name);

                List<string> rowsInFile = readFile(infile);

                if(rowsInFile.Count == 0)
                {
                    LoggMgr.WriteToLogg("Found old file: " + infile + ", this file is moved to output!");
                    string outFolder = Properties.Settings.Default.outfolder;

                    File.Move(infile, Path.Combine(outFolder, Path.GetFileName(infile)));
                }

                mTotalRowsAllFiles.AddRange(rowsInFile);

                int rowcountinfile = 0;

                foreach(string line in rowsInFile)
                {
                    rowcountinfile++;
                    string[] fields = line.Split(',');

                    if(fields[0] == "DEL")
                    {
                        ArticleDTO al = getDeliverPlan(fields);

                        if(al != null)
                        {
                            result.Add(al);
                        }
                    }

                }
            }
            catch (Exception e)
            {
                LoggMgr.WriteToLogg("ERR " + System.Reflection.MethodBase.GetCurrentMethod().Name, e);
            }

            return result;
        }

        private static ArticleDTO getDeliverPlan(string[] fields)
        {
            ArticleDTO artLev = new ArticleDTO();
            int daysAhead = Properties.Settings.Default.daysAhead;
            DateTime dateDaysAhead = DateTime.Now.AddDays(daysAhead);

            //check date is withing days ahead limit
            string deldate = fields[2].Trim('\"');
            if (deldate.Length == 8)
            {
                deldate = deldate.Substring(2, 6);
            }

            DateTime levDate = DateTime.ParseExact(deldate, "yyMMdd", CultureInfo.CurrentCulture);

            if (levDate <= dateDaysAhead)
            {
                CustomerDTO customer = getCustomer(fields[1].Trim('\"').ToString());
                ProductDTO product = getProduct(fields[1].Trim('\"').ToString());

                artLev.Levtid_garp = "";
                artLev.Antal_garp = -1;
                artLev.Saldoefter = -1;
                artLev.Ordernr = "";
                artLev.Radnr_garp = "";

 
                if (product?.Description != "NOT FOUND" && customer.Name != "NOT FOUND")
                {
                    artLev.Statustext = "";
                    artLev.Statustype = -1;
                }
                else
                {
                    if (customer.Name == "NOT FOUND")
                    {
                        artLev.Statustext = Program.g_STATUS_CUST_NOT_FOUND;
                        artLev.Statustype = Program.g_STATUS_CUST_NOT_FOUND_NUM;
                    }
                    else if (product?.Description == "NOT FOUND")
                    {
                        artLev.Statustext = Program.g_STATUS_ART_NOT_FOUND;
                        artLev.Statustype = Program.g_STATUS_ART_NOT_FOUND_NUM;
                    }
                    //if (string.IsNullOrEmpty(customer?.CustomerNo))
                    //{
                    //    artLev.Statustext = Program.g_STATUS_CUST_NOT_FOUND;
                    //    artLev.Statustype = Program.g_STATUS_CUST_NOT_FOUND_NUM;
                    //}
                }

                artLev.Atgardtext = "";
                artLev.Atgardtype = -1;
                artLev.Kundnr_garp = customer.CustomerNo;
                artLev.Kundnr_xml = customer.CustomerNoFromFile;
                artLev.Kundname_xml = customer.Name;
                artLev.Artnr_xml = product.ProductNo;
                artLev.Levtid_xml = deldate;

                int thisweek = GetWeekNumber(levDate);
                string strWeek = "";

                if (thisweek < 10)
                {
                    strWeek = levDate.Year.ToString().Substring(2, 2) + "0" + thisweek.ToString();
                }
                else
                {
                    strWeek = levDate.Year.ToString().Substring(2, 2) + thisweek.ToString();
                }
                artLev.Week_xml = strWeek;

                string antalxml = fields[4].Trim('\"').Replace(".", ",");
                double antal = 0;

                if (Double.TryParse(antalxml, out antal))
                {
                    artLev.Rowid_xml = -1;
                    artLev.Antal_xml = antal;
                }
                else
                {
                    LoggMgr.WriteToLogg("ERR wrong dataformat in file, qty: " + antalxml + " " + System.Reflection.MethodBase.GetCurrentMethod().Name, null);
                }
            }
            else
            {
                artLev = null;
            }

            return artLev;
        }

        /// <summary>
        /// 
        /// Get customer from preloaded customerlist. If not CustomerNo match we seek through EAN addresses om customer.
        /// 
        /// </summary>
        /// <param name="fields"></param>
        /// <returns></returns>
        private static CustomerDTO getCustomer(string segmentId)
        {
            CustomerDTO result = new CustomerDTO();

            try
            {
                SegmentKey found = mCustomerList.Find(p => p.SegmentId == segmentId);

                if (found != null)
                {
                    // Key is Garp CustomerNo
                    result = DataHandler.getCustomerFromGISList(found.Key);
                    
                    if(result == null)
                    {
                        result = new CustomerDTO();

                        result.CustomerNo = found.Key;
                        result.CustomerNoFromFile = found.KeyInFile;
                        result.Name = "NOT FOUND";
                    }
                    else
                    {
                        result.CustomerNoFromFile = found.KeyInFile;
                    }
                }
            }
            catch (Exception e)
            {
                LoggMgr.WriteToLogg("Error getting Customer on SegmentId: " + segmentId);
            }

            return result;
        }

        private static ProductDTO getProduct(string segmentId)
        {
            ProductDTO result = new ProductDTO();
            string m_product = "";

            try
            {
                m_product = mProductList.Find(p => p.SegmentId == segmentId).Key;

                if (!string.IsNullOrEmpty(m_product))
                {
                    result = DataHandler.getProductFromGISList(m_product);

                    if(result == null)
                    {
                        result = new ProductDTO();

                        result.ProductNo = m_product;
                        result.Description = "NOT FOUND";
                    }
                }
            }
            catch (Exception e)
            {
                LoggMgr.WriteToLogg("Error getting Product on SegmentId: " + segmentId);
            }

            return result;
        }

        /// <summary>
        /// Read file and add to list of lines and also update customer and product list with index for each segment
        /// </summary>
        /// <param name="infile"></param>
        private static List<string> readFile(string infile)
        {
            string line = "";
            string[] splittedLine = null;
            List<string> result = new List<string>();

            FileParser.mCustomerList.Clear();
            FileParser.mProductList.Clear();

            using(StreamReader reader = new StreamReader(infile))
            {
                while ((line = reader.ReadLine()) != null)
                {
                    result.Add(line);

                    splittedLine = line.Split(',');

                    if(splittedLine[0].Equals("HEAD"))
                    {
                        fillProductListFromFile(splittedLine, infile);

                        // Check if already readed this product in another file, if so we abort, this is an old file
                        if (isProductIsReadedFromAnotherFile(splittedLine[2].Trim('\"'), infile))
                        {
                            result.Clear();
                            break;
                        }

                        fillCustomerListFromFile(splittedLine);
                    }
                    else if (splittedLine[0].Equals("DOC"))
                    {
                        fillCustomerListFromFile(splittedLine);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Add customer to list with index on segment
        /// </summary>
        /// <param name="line"></param>
        private static void fillCustomerListFromFile(string[] line)
        {
            try
            {
                if(line[0].Equals("HEAD"))
                {
                    if(!string.IsNullOrEmpty(line[12].Trim('\"')))
                    {
                        SegmentKey segkey = new SegmentKey { SegmentId = line[1].Trim('\"'), Key = DataHandler.getCustomerFromGISList(line[12].Trim('\"'))?.CustomerNo, KeyInFile = line[12].Trim('\"') };
                        mCustomerList.Add(segkey);

                        if (!DataHandler.CustomersFoundInFiles.Contains(segkey.Key))  //(line[12].Trim('\"')))
                        {
                            //DataHandler.CustomersFoundInFiles.Add(line[12].Trim('\"'));
                            DataHandler.CustomersFoundInFiles.Add(segkey.Key);
                        }
                    }
                }
                else if(line[0].Equals("DOC"))
                {
                    if (!string.IsNullOrEmpty(line[5].Trim('\"')))
                    {
                        SegmentKey segkey = new SegmentKey { SegmentId = line[1].Trim('\"'), Key = DataHandler.getCustomerFromGISList(line[5].Trim('\"'))?.CustomerNo, KeyInFile = line[5].Trim('\"') };
                        //mCustomerList.Add(new SegmentKey { SegmentId = line[1].Trim('\"'), Key = line[5].Trim('\"') });
                        mCustomerList.Add(segkey);

                        if (!DataHandler.CustomersFoundInFiles.Contains(segkey.Key)) //(line[5].Trim('\"')))
                        {
                            //DataHandler.CustomersFoundInFiles.Add(line[5].Trim('\"'));
                            DataHandler.CustomersFoundInFiles.Add(segkey.Key);
                        }
                    }
                }
            }
            catch(Exception e)
            {
                LoggMgr.WriteToLogg("Error adding Customer to list", e);
            }
        }

        /// <summary>
        /// Add product with index on segment
        /// </summary>
        /// <param name="line"></param>
        private static void fillProductListFromFile(string[] line, string file)
        {
            try
            {
                if (line[0].Equals("HEAD"))
                {
                    if (!string.IsNullOrEmpty(line[2]))
                    {
                        mProductList.Add(new SegmentKey { SegmentId = line[1].Trim('\"'), Key = line[2].Trim('\"') });
                    }

                    // If product newer is readed before, we add it
                    if (DataHandler.ProductsFoundInFiles.Find(p=>p.ProductNo == line[2].Trim('\"')) == null)
                    {
                        ProductInFile pin = new ProductInFile();
                        pin.ProductNo = line[2].Trim('\"');
                        pin.File = file;
                        DataHandler.ProductsFoundInFiles.Add(pin);
                    }
                }
            }
            catch (Exception e)
            {
                LoggMgr.WriteToLogg("Error adding Product to list", e);
            }
        }

        private static bool isProductIsReadedFromAnotherFile(string productno, string file)
        {
            bool result;

            try
            {
                // If product newer is readed before, we add it
                if (DataHandler.ProductsFoundInFiles.Find(p => p.ProductNo == productno && p.File == file) == null)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception e)
            {
                LoggMgr.WriteToLogg("Error in isProductIsReadedFromAnotherFile", e);
                result = false;
            }

            return result;
        }

        public static int GetWeekNumber(DateTime date)
        {
            var cultureInfo = CultureInfo.CurrentCulture;
            var calendar = cultureInfo.Calendar;

            var calendarWeekRule = cultureInfo.DateTimeFormat.CalendarWeekRule;
            var firstDayOfWeek = cultureInfo.DateTimeFormat.FirstDayOfWeek;
            var lastDayOfWeek = cultureInfo.LCID == 1033 //En-us
                                ? DayOfWeek.Saturday
                                : DayOfWeek.Sunday;

            var lastDayOfYear = new DateTime(date.Year, 12, 31);

            var weekNumber = calendar.GetWeekOfYear(date, calendarWeekRule, firstDayOfWeek);

            //Check if this is the last week in the year and it doesn`t occupy the whole week
            return weekNumber == 53 && lastDayOfYear.DayOfWeek != lastDayOfWeek
                   ? 1
                   : weekNumber;
        }
    }

    public class SegmentKey
    {
        public string SegmentId { get; set; }
        public string Key { get; set; }
        public string KeyInFile { get; set; }
    }
}




