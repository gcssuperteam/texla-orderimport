﻿namespace GarpOrderMgr
{
    partial class Mainform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Mainform));
            this.dgvOrder = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerNr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CustName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.artNr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Week = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Levtid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Antal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Atgard = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Andra = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Plan = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cmdSave = new System.Windows.Forms.Button();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.lblInfo = new System.Windows.Forms.Label();
            this.tableLayoutPanelSearchCust = new System.Windows.Forms.TableLayoutPanel();
            this.cmbCustomer = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPaneltop = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelArtSearch = new System.Windows.Forms.TableLayoutPanel();
            this.lblArticle = new System.Windows.Forms.Label();
            this.cmbArticle = new System.Windows.Forms.ComboBox();
            this.lblCustomer = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrder)).BeginInit();
            this.tableLayoutPanel.SuspendLayout();
            this.tableLayoutPanelSearchCust.SuspendLayout();
            this.tableLayoutPaneltop.SuspendLayout();
            this.tableLayoutPanelArtSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvOrder
            // 
            this.dgvOrder.AllowUserToAddRows = false;
            this.dgvOrder.AllowUserToDeleteRows = false;
            this.dgvOrder.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvOrder.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvOrder.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgvOrder.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(114)))), ((int)(((byte)(196)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvOrder.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvOrder.ColumnHeadersHeight = 40;
            this.dgvOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvOrder.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.customerNr,
            this.CustName,
            this.artNr,
            this.Week,
            this.Levtid,
            this.Antal,
            this.OrderNo,
            this.Status,
            this.Atgard,
            this.Andra,
            this.Plan});
            this.dgvOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvOrder.EnableHeadersVisualStyles = false;
            this.dgvOrder.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(170)))), ((int)(((byte)(219)))));
            this.dgvOrder.Location = new System.Drawing.Point(13, 146);
            this.dgvOrder.MultiSelect = false;
            this.dgvOrder.Name = "dgvOrder";
            this.dgvOrder.RowHeadersVisible = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(240)))), ((int)(((byte)(249)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvOrder.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvOrder.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvOrder.Size = new System.Drawing.Size(1175, 571);
            this.dgvOrder.TabIndex = 0;
            this.dgvOrder.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvOrder_OnCellMouseUp);
            this.dgvOrder.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvOrder_CellPainting);
            this.dgvOrder.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvOrder_OnCellValueChanged);
            this.dgvOrder.SelectionChanged += new System.EventHandler(this.dgvOrder_SelectionChanged);
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ID.Visible = false;
            // 
            // customerNr
            // 
            this.customerNr.FillWeight = 58.81211F;
            this.customerNr.HeaderText = "CustNr";
            this.customerNr.Name = "customerNr";
            // 
            // CustName
            // 
            this.CustName.FillWeight = 60F;
            this.CustName.HeaderText = "CustName";
            this.CustName.Name = "CustName";
            // 
            // artNr
            // 
            this.artNr.FillWeight = 58.81211F;
            this.artNr.HeaderText = "ItemNo";
            this.artNr.Name = "artNr";
            // 
            // Week
            // 
            this.Week.FillWeight = 58.81211F;
            this.Week.HeaderText = "Week";
            this.Week.Name = "Week";
            // 
            // Levtid
            // 
            this.Levtid.FillWeight = 59.44652F;
            this.Levtid.HeaderText = "Del.date";
            this.Levtid.Name = "Levtid";
            this.Levtid.ReadOnly = true;
            // 
            // Antal
            // 
            this.Antal.FillWeight = 59.44652F;
            this.Antal.HeaderText = "Quantity";
            this.Antal.Name = "Antal";
            this.Antal.ReadOnly = true;
            // 
            // OrderNo
            // 
            this.OrderNo.FillWeight = 59.44652F;
            this.OrderNo.HeaderText = "Order";
            this.OrderNo.Name = "OrderNo";
            this.OrderNo.ReadOnly = true;
            // 
            // Status
            // 
            this.Status.FillWeight = 59.44652F;
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            // 
            // Atgard
            // 
            this.Atgard.FillWeight = 59.44652F;
            this.Atgard.HeaderText = "Action";
            this.Atgard.Name = "Atgard";
            this.Atgard.ReadOnly = true;
            // 
            // Andra
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.NullValue = "0";
            this.Andra.DefaultCellStyle = dataGridViewCellStyle2;
            this.Andra.FalseValue = "0";
            this.Andra.FillWeight = 38.22787F;
            this.Andra.HeaderText = "Change";
            this.Andra.Name = "Andra";
            this.Andra.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Andra.TrueValue = "1";
            // 
            // Plan
            // 
            this.Plan.FalseValue = "0";
            this.Plan.FillWeight = 36.16663F;
            this.Plan.HeaderText = "Plan";
            this.Plan.Name = "Plan";
            this.Plan.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Plan.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Plan.TrueValue = "1";
            // 
            // cmdSave
            // 
            this.cmdSave.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cmdSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(80)))));
            this.cmdSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSave.ForeColor = System.Drawing.Color.White;
            this.cmdSave.Location = new System.Drawing.Point(13, 733);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(200, 55);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save to garp";
            this.cmdSave.UseVisualStyleBackColor = false;
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 1;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.Controls.Add(this.lblInfo, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.dgvOrder, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.cmdSave, 0, 3);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel.RowCount = 4;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 73F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(1201, 811);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // lblInfo
            // 
            this.lblInfo.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfo.Location = new System.Drawing.Point(13, 113);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(591, 20);
            this.lblInfo.TabIndex = 1;
            // 
            // tableLayoutPanelSearchCust
            // 
            this.tableLayoutPanelSearchCust.ColumnCount = 1;
            this.tableLayoutPanelSearchCust.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelSearchCust.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelSearchCust.Controls.Add(this.cmbCustomer, 0, 1);
            this.tableLayoutPanelSearchCust.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanelSearchCust.Location = new System.Drawing.Point(603, 12);
            this.tableLayoutPanelSearchCust.Name = "tableLayoutPanelSearchCust";
            this.tableLayoutPanelSearchCust.RowCount = 2;
            this.tableLayoutPanelSearchCust.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelSearchCust.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelSearchCust.Size = new System.Drawing.Size(239, 73);
            this.tableLayoutPanelSearchCust.TabIndex = 2;
            // 
            // cmbCustomer
            // 
            this.cmbCustomer.DisplayMember = "key";
            this.cmbCustomer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCustomer.FormattingEnabled = true;
            this.cmbCustomer.Location = new System.Drawing.Point(3, 39);
            this.cmbCustomer.Name = "cmbCustomer";
            this.cmbCustomer.Size = new System.Drawing.Size(233, 28);
            this.cmbCustomer.TabIndex = 1;
            this.cmbCustomer.ValueMember = "key";
            this.cmbCustomer.SelectedIndexChanged += new System.EventHandler(this.cmbCustomer_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Customer";
            // 
            // tableLayoutPaneltop
            // 
            this.tableLayoutPaneltop.ColumnCount = 3;
            this.tableLayoutPaneltop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPaneltop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPaneltop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPaneltop.Controls.Add(this.tableLayoutPanelSearchCust, 1, 0);
            this.tableLayoutPaneltop.Controls.Add(this.tableLayoutPanelArtSearch, 2, 0);
            this.tableLayoutPaneltop.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPaneltop.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPaneltop.Name = "tableLayoutPaneltop";
            this.tableLayoutPaneltop.Padding = new System.Windows.Forms.Padding(9);
            this.tableLayoutPaneltop.RowCount = 1;
            this.tableLayoutPaneltop.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPaneltop.Size = new System.Drawing.Size(1201, 97);
            this.tableLayoutPaneltop.TabIndex = 3;
            // 
            // tableLayoutPanelArtSearch
            // 
            this.tableLayoutPanelArtSearch.ColumnCount = 1;
            this.tableLayoutPanelArtSearch.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelArtSearch.Controls.Add(this.lblArticle, 1, 0);
            this.tableLayoutPanelArtSearch.Controls.Add(this.cmbArticle, 1, 1);
            this.tableLayoutPanelArtSearch.Location = new System.Drawing.Point(898, 12);
            this.tableLayoutPanelArtSearch.Name = "tableLayoutPanelArtSearch";
            this.tableLayoutPanelArtSearch.RowCount = 2;
            this.tableLayoutPanelArtSearch.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelArtSearch.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelArtSearch.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelArtSearch.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelArtSearch.Size = new System.Drawing.Size(200, 73);
            this.tableLayoutPanelArtSearch.TabIndex = 4;
            // 
            // lblArticle
            // 
            this.lblArticle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblArticle.AutoSize = true;
            this.lblArticle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArticle.Location = new System.Drawing.Point(3, 16);
            this.lblArticle.Name = "lblArticle";
            this.lblArticle.Size = new System.Drawing.Size(53, 20);
            this.lblArticle.TabIndex = 3;
            this.lblArticle.Text = "Article";
            // 
            // cmbArticle
            // 
            this.cmbArticle.DisplayMember = "key";
            this.cmbArticle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbArticle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbArticle.FormattingEnabled = true;
            this.cmbArticle.Location = new System.Drawing.Point(3, 39);
            this.cmbArticle.Name = "cmbArticle";
            this.cmbArticle.Size = new System.Drawing.Size(194, 28);
            this.cmbArticle.TabIndex = 2;
            this.cmbArticle.ValueMember = "key";
            this.cmbArticle.SelectedIndexChanged += new System.EventHandler(this.cmbArticle_SelectedIndexChanged);
            // 
            // lblCustomer
            // 
            this.lblCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblCustomer.AutoSize = true;
            this.lblCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomer.Location = new System.Drawing.Point(603, 68);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(78, 20);
            this.lblCustomer.TabIndex = 5;
            this.lblCustomer.Text = "Customer";
            // 
            // Mainform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1201, 811);
            this.Controls.Add(this.tableLayoutPaneltop);
            this.Controls.Add(this.tableLayoutPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1200, 850);
            this.Name = "Mainform";
            this.Text = "Garp order management";
            this.Load += new System.EventHandler(this.Mainform_Load);
            this.Resize += new System.EventHandler(this.Mainform_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrder)).EndInit();
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanelSearchCust.ResumeLayout(false);
            this.tableLayoutPanelSearchCust.PerformLayout();
            this.tableLayoutPaneltop.ResumeLayout(false);
            this.tableLayoutPanelArtSearch.ResumeLayout(false);
            this.tableLayoutPanelArtSearch.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvOrder;
        private System.Windows.Forms.Button cmdSave;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.ComboBox cmbArticle;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPaneltop;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelArtSearch;
        private System.Windows.Forms.Label lblArticle;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelSearchCust;
        private System.Windows.Forms.Label lblCustomer;
        private System.Windows.Forms.ComboBox cmbCustomer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerNr;
        private System.Windows.Forms.DataGridViewTextBoxColumn CustName;
        private System.Windows.Forms.DataGridViewTextBoxColumn artNr;
        private System.Windows.Forms.DataGridViewTextBoxColumn Week;
        private System.Windows.Forms.DataGridViewTextBoxColumn Levtid;
        private System.Windows.Forms.DataGridViewTextBoxColumn Antal;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn Atgard;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Andra;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Plan;
    }
}

